var pg = require('pg');
var config = require('config');
var express = require('express');
var session = require('express-session')
var RedisStore = require('connect-redis')(session);
var app = express();

app.use(session({
    store: new RedisStore({ttl: 86400}),
    secret: 'ejboocctm',
    resave: false,
//    cookie: {domain: '.' + config.host},
    saveUninitialized: true
}));
app.use(function(request, response, next) {
    var render = response.render;
    response.render = function(view, param) {
        if (!param) {
            param = {};
        }

        param.session = request.session;
        param.user = request.user;
        param.client = request.client;
        param.env = process.env;

        response.render = render;
        response.render(view, param);
    }

    if (request.session.userId) {
        if (request.session.userId == 'root@cctm.kr') {
            pg.connect(config.db, function(error, connection, done) {
                connection.query('SELECT u.id, u.email FROM "user" u, client c WHERE u.id IN (SELECT user_id FROM client_user WHERE client_id = $1) LIMIT 1', [request.client.id], function(error, result) {
                    if (result.rows[0]) {
                        request.user = result.rows[0];
                    }
                    done();
                    next();
                });
            });
        } else {
            pg.connect(config.db, function(error, connection, done) {
                connection.query('SELECT u.id, u.email FROM "user" u, client c WHERE u.id = $1 AND u.id IN (SELECT user_id FROM client_user WHERE client_id = $2)', [request.session.userId, request.client.id], function(error, result) {
                    if (result.rows[0]) {
                        request.user = result.rows[0];

                        if (request.client.id == request.session.clientId) {
                            done();
                            next();
                            return;
                        }

                        var now = new Date();
                        now.setHours(0);
                        now.setMinutes(0);
                        now.setSeconds(0);
                        now.setMilliseconds(0);

                        request.session.clientId = request.client.id;

                        var type = 'owner';
                        if (request.user.email == 'check@cctm.kr') {
                            type = 'inspector';
                        }

                        connection.query('INSERT INTO visit (user_id, client_id, type) SELECT id, $2, $3 FROM "user" WHERE id = $1 AND id NOT IN (SELECT user_id FROM visit WHERE user_id = $1 AND client_id = $2 AND type = $3 AND date >= $4)', [request.user.id, request.client.id, type, now], function(error, result) {
                            done();
                            next();
                        });
                    } else {
                        done();
                        next();
                    }
                });
            });
        }
    } else {
        next();
    }
});
app.use(require('./controllers/user'));
app.use(require('./controllers/index'));
app.use(require('./controllers/live'));
app.use(require('./controllers/file'));
app.use(require('./controllers/management'));
app.use(require('./controllers/leakage'));
app.use(require('./controllers/policy'));
app.use(function(request, response, next) {
    if (request.path.match(/.html$/)) {
        response.redirect('/');
    } else {
        next();
    }
});
app.use('/jwplayer', require('jwplayer'));
app.use('/player', require('player'));
app.use(require('epona'));

module.exports = app;
