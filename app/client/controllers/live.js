var pg = require('pg');
var async = require('async');
var config = require('config');
var logger = require('logger');
var heredoc = require('heredoc');
var express = require('express');
var router = module.exports = express.Router();

router.get('/live/list', function(request, response, next) {
    var connection = null;
    var done = null;

    var cameras = null;
    var format = 'jpeg';
    var intranet = false;

    async.waterfall([
        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

            connection.query(heredoc(function() {/*
                SELECT camera.*, settop.private_ip AS private_ip, settop.public_ip, settop.id AS settop_id
                FROM camera, recorder, settop
                WHERE
                    camera.recorder_id = recorder.id
                    AND
                    recorder.settop_id = settop.id
                    AND
                    settop.client_id = $1
                ORDER BY camera.recorder_id, camera.channel
            */}), [request.client.id], next);
        },

        function(result, next) {
            cameras = result.rows;
            async.eachSeries(cameras, function(camera, next) {
                var domain = '';
                if (camera.public_ip == request.ip) {
                    domain = camera.private_ip.replace(/\./g, '-') + '.cctm.co.kr';
                    if (request.user) {
                        format = 'm3u8';
                    } else {
                        format = 'jpeg';
                    }
                    intranet = true;
                } else {
                    domain = camera.settop_id + '.cctm.co.kr';
                    if (request.user && request.client.option.network) {
                        format = 'm3u8';
                    } else {
                        format = 'jpeg';
                    }
                    intranet = false;
                }

                if (request.user && request.session.userId == 'root@cctm.kr') {
                    format = 'm3u8';
                    request.client.option.network = true;
                }

                if (request.user && request.user.email == 'checker@cctm.kr') {
                    format = 'jpeg';
                    request.client.option.network = false;
                }

                if (format == 'jpeg') {
                    camera.url = 'https://' + domain + '/' + camera.recorder_id + '/' + camera.id + '/sub.jpg';
                } else {
                    camera.url = 'https://' + domain + '/' + camera.recorder_id + '/' + camera.id + '/sub.m3u8';
                }

                //require('crypto').randomBytes(16).toString('base64')
                setImmediate(next);
            }, next);
        }
    ], function(error) {
        if (done) {
            done();
        }

        if (error) {
            logger.error(error);
            response.redirect('/');
        } else {
            response.render('live/list.html', {cameras: cameras, format: format, intranet: intranet, ip: request.ip});
        }
    });
});

router.get('/live/play', function(request, response, next) {
    response.render('live/play.html', {recorderId: request.query.recorderId, cameraId: request.query.cameraId, name: request.query.name});
});
