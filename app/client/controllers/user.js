var pg = require('pg');
var net = require('net');
var md5 = require('md5');
var async = require('async');
var config = require('config');
var logger = require('logger');
var heredoc = require('heredoc');
var express = require('express');
var router = module.exports = express.Router();

router.get('/user/login', function(request, response, next) {
    if (request.user) {
        response.redirect('/');
    } else {
        response.render('user/login.html', {email: request.cookies.email});
    }
});

router.post('/user/login', function(request, response, next) {
    var connection = null;
    var done = null;

    var user = null;
    var email = request.body.email.trim().toLowerCase();
    var password = request.body.password.trim();

    async.waterfall([
        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

            connection.query(heredoc(function() {/*
                SELECT u.id
                FROM "user" u, client_user cu, client c
                WHERE
                    u.id = cu.user_id
                    AND
                    c.id = cu.client_id
                    AND
                    lower(u.email) = $1
                    AND
                    u.password = $2
                    AND
                    cu.type IN ('owner', 'inspector')
                    AND
                    c.id = $3
                LIMIT 1
            */}), [email, md5(password), request.client.id], next);
        },

        function(result, next) {
            if (!result.rows.length) {
                next(null, null);
            } else {
                user = result.rows[0];

                var agent = request.get('user-agent');
                var ip = request.headers['x-forwarded-for'] || request.headers['x-real-ip'] || request.connection.remoteAddress || null;
                ip = ip.split(',').shift();
                ip = ip.split(':').shift();
                if (!net.isIP(ip)) {
                    ip = '0.0.0.0';
                }

                connection.query(heredoc(function() {/*
                    INSERT INTO login(user_id, ip, agent)
                    VALUES ($1, $2, $3)
                */}), [user.id, ip, agent], next);
            }
        }
    ], function(error) {
        if (done) {
            done();
        }

        if (error) {
            logger.error(error);
            response.redirect('/');
        } else {
            if (user) {
                if (request.body.save) {
                    response.cookie('email', email, {maxAge: 315360000000});
                } else {
                    response.clearCookie('email');
                }
                request.session.userId = user.id;
            }
            response.jsonp(user);
        }
    });
});

router.get('/user/logout', function(request, response, next) {
    delete request.session.logged;
    delete request.session.userId;
    delete request.session.clientId;
    response.redirect('/');
});

router.get('/user/edit', function(request, response, next) {
    var id = null;
    var user = request.user;
    if (user) {
        id = user.id;
    }
    if (!id) {
        response.redirect('/');
    }

    var connection = null;
    var done = null;

    async.waterfall([
        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

            connection.query(heredoc(function() {/*
                SELECT c.name, c.option
                FROM "user" u, client_user cu, client c
                WHERE
                    u.id = cu.user_id
                    AND
                    c.id = cu.client_id
                    AND
                    cu.type = 'owner'
                    AND
                    u.id = $1
                    AND
                    c.id = $2
                LIMIT 1
            */}), [id, request.client.id], next);
        },

        function(result, next) {
            if (!result.rows.length) {
                next(null, null);
            } else {
                next(null, result.rows[0]);
            }
        }
    ], function(error, client) {
        if (done) {
            done();
        }

        if (error) {
            logger.error(error);
            response.redirect('/');
        } else {
            response.render('user/edit.html', {client: client});
        }
    });
});

router.post('/user/save', function(request, response, next) {
    var id = null;
    var user = request.user;
    if (user) {
        id = user.id;
    }
    if (!id) {
        response.redirect('/');
    }

    var connection = null;
    var done = null;

    var type = request.body.type;

    async.waterfall([
        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

            if (type == 'password') {
                connection.query(heredoc(function() {/*
                    UPDATE "user"
                    SET password = md5($1)
                    WHERE id = $2
                */}), [request.body.password, id], next);
            } else if (type == 'client') {
                connection.query(heredoc(function() {/*
                    UPDATE client
                    SET name = $1
                    WHERE id = $2 AND id IN (SELECT client_id FROM client_user WHERE type = 'owner' AND user_id = $3)
                */}), [request.body.name, request.client.id, id], next);
            } else if (type == 'option') {
                connection.query(heredoc(function() {/*
                    SELECT option
                    FROM client
                    WHERE id = $1
                */}), [request.client.id], function(error, result) {
                    if (error) {
                        return next(error);
                    }

                    var client = result.rows[0];
                    if (!client) {
                        return next(null, null);
                    }

                    var option = client.option;

                    option.live.column = request.body.column;
                    option.live.row = request.body.row;

                    connection.query(heredoc(function() {/*
                        UPDATE client
                        SET option = $1
                        WHERE id = $2 AND id IN (SELECT client_id FROM client_user WHERE type = 'owner' AND user_id = $3)
                    */}), [option, request.client.id, id], next);
                });
            } else {
                next(null, null);
            }
        }
    ], function(error, client) {
        if (done) {
            done();
        }

        if (error) {
            logger.error(error);
            response.redirect('/');
        } else {
            response.jsonp({type: type});
        }
    });
});
