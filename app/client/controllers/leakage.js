var express = require('express');
var router = module.exports = express.Router();

router.get('/leakage/list', function(request, response) {
    response.render('leakage/list.html');
});

router.get('/leakage/show', function(request, response) {
    response.render('leakage/show.html');
});

router.get('/leakage/edit', function(request, response) {
    response.render('leakage/edit.html');
});
