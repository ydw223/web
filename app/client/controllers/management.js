var pg = require('pg');
var xlsx = require('xlsx');
var async = require('async');
var moment = require('moment');
var logger = require('logger');
var config = require('config');
var express = require('express');
var router = module.exports = express.Router();

router.get('/management/list', function(request, response) {
    var client = request.client;

    var connection = null;
    var done = null;

    async.waterfall([
        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

            connection.query(`
                SELECT *
                FROM management_register
                WHERE
                    client_id = $1
                ORDER BY management_register.date
            `, [client.id], next);
        }
    ], function(error, result) {
        if (done) {
            done();
        }
        if (error) {
            logger.error(error);
            response.redirect('/');
        } else {
            response.render('management/list.html', {managementRegisters: result.rows});
        }
    });

    /*
    var start = request.query.start;
    var end = request.query.end;

    var startDate = new Date(2015, 11, 1);
    startDate.setHours(0);
    startDate.setMinutes(0);
    startDate.setSeconds(0);
    if (start) {
        startDate = moment(start).toDate();
    }

    var endDate = new Date(startDate.getFullYear(), startDate.getMonth() + 1, startDate.getDate() - 1);
    if (end) {
        endDate = moment(end).toDate();
    }
    */
});

router.get('/management/inspection/show', function(request, response) {
    response.render('management/inspection/show.html');
});

router.get('/management/inspection/edit', function(request, response) {
    response.render('management/inspection/edit.html');
});

router.get('/management/destruction/show', function(request, response) {
    response.render('management/destruction/show.html');
});

router.get('/management/destruction/edit', function(request, response) {
    response.render('management/destruction/edit.html');
});

router.get('/management/inspection/download', function(request, response) {
    /*
    var start = request.query.start;
    var end = request.query.end;

    var startDate = new Date();
    startDate.setHours(0);
    startDate.setMinutes(0);
    startDate.setSeconds(0);
    if (start) {
        startDate = moment(start).toDate();
    }

    var endDate = new Date(startDate.getFullYear(), startDate.getMonth() + 1, startDate.getDate() - 1);
    if (end) {
        endDate = moment(end).toDate();
    }
    */

    var startDate = new Date(2015, 11, 1);
    var endDate = new Date();
    endDate.setHours(0);
    endDate.setMinutes(0);
    endDate.setSeconds(0);

    var client = request.client
    var connection = null;
    var done = null;

    async.waterfall([
        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

            connection.query(`
                SELECT management_register.*, client.representative
                FROM management_register, client
                WHERE
                    client.id = management_register.client_id
                    AND
                    client.id = $1
                ORDER BY management_register.date
            `, [client.id], next);
        },

        function(result, next) {
            if (done) {
                done();
            }

            var count = 1;
            var index = 6;
            var rows = [];
            var cells = {'A': {}, 'B': {}, 'C': {}, 'D': {}, 'E': {}, 'F': {}, 'G': {}, 'H': {}, 'I': {}, 'J': {}, 'K': {}, 'L': {}};

            cells.A[3] = {value: client.name};
            cells.A[4] = {value: moment(startDate).format('YYYY년 MM월 DD일') + ' ~ ' + moment(endDate).format('YYYY년 MM월 DD일')};
            cells.E[3] = {value: '관리책임자: ' + client.representative + ' 원장'};
            cells.E[4] = {value: '관리담당자: ' + client.representative + ' 원장'};

            async.eachSeries(result.rows, function(row, next) {
                cells.A[index] = {value: count, duplicate: 'A6'};
                cells.B[index] = {value: row.type, duplicate: 'B6'};
                cells.C[index] = {value: moment(row.date).format('YYYY-MM-DD HH:mm:ss'), duplicate: 'C6'};
                cells.D[index] = {value: row.content, duplicate: 'D6'};
                cells.E[index] = {value: row.purpose, duplicate: 'E6'};
                cells.F[index] = {value: row.requester, duplicate: 'F6'};
                cells.G[index] = {value: row.basis, duplicate: 'G6'};
                cells.H[index] = {value: row.form, duplicate: 'H6'};
                cells.I[index] = {value: row.schedule, duplicate: 'I6'};
                cells.J[index] = {value: row.place, duplicate: 'G6'};
                cells.K[index] = {value: row.manager, duplicate: 'K6'};
                cells.L[index] = {value: row.note, duplicate: 'L6'};

                rows.push({number: index, height: -1});

                count++;
                index++;
                setImmediate(next);
            }, function() {
                if (!rows.length) {
                    rows.push({number: 6, action: 'remove'});
                }
                var workbook = {
                    template: __dirname + '/../excels/개인영상정보관리대장.xls',
                    format: 'excel5',
                    sheets: [
                        {title: '개인영상정보관리대장', rows: rows, cells: cells, selected: 'A1'}
                    ]
                };
                next(null, workbook);
            });
        }
    ], function(error, workbook) {
        if (error) {
            logger.error(error);
            response.redirect('/');
        } else {
            var fileName = encodeURIComponent(client.name + '_개인영상정보관리대장_' + moment(startDate).format('YYYYMMDD') + '_' + moment(endDate).format('YYYYMMDD') + '.xls');
            response.setHeader('Cache-Control', 'max-age=0');
            response.setHeader('Content-Type', 'application/vnd.ms-excel; charset=utf-8');
            response.setHeader('Content-Disposition', 'attachment; filename="' + fileName + '"');

            var stream = xlsx.createReadStream(workbook);
            stream.pipe(response);
            stream.on('error', function(error) {
                logger.error(error);
                response.status(500).end();
            });
        }
    });
});

router.get('/management/destruction/download', function(request, response) {
    response.status(404).end();
});
