var pg = require('pg');
var util = require('util');
var async = require('async');
var moment = require('moment');
var config = require('config');
var logger = require('logger');
var heredoc = require('heredoc');
var express = require('express');
var router = module.exports = express.Router();

router.get('/file/list', function(request, response, next) {
    var connection = null;
    var done = null;

    var files = null;
    var domain = null;
    var camera = null;
    var cameras = null;

    var cameraId = request.query.cameraId;
    var date = request.query.date;

    var intranet = false;

    if (!date) {
        date = new Date();
    } else {
        date = moment(date, 'YYYY-MM-DD').toDate();
    }

    async.waterfall([
        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

            connection.query(heredoc(function() {/*
                SELECT camera.*, settop.id AS settop_id, settop.public_ip, settop.private_ip
                FROM camera, recorder, settop, client
                WHERE
                    camera.recorder_id = recorder.id
                    AND
                    recorder.settop_id = settop.id
                    AND
                    settop.client_id = client.id
                    AND
                    client.id = $1
                ORDER BY camera.recorder_id, camera.channel
            */}), [request.client.id], next);
        },

        function(result, next) {
            var index = 0;
            var fields = [];
            var values = [];
            var startDate = moment(date).format('YYYY-MM-DD 00:00:00Z');
            var endDate = moment(date).add(1, 'day').format('YYYY-MM-DD 00:00:00Z');

            cameras = result.rows;

            async.eachSeries(result.rows, function(row, next) {
                index++;
                fields.push('$' + index);
                values.push(row.id);

                if (camera === null) {
                    if (cameraId) {
                        if (row.id == cameraId) {
                            camera = row;
                        }
                    } else {
                        if (row.channel == 1) {
                            camera = row;
                        }
                    }
                }

                if (row.public_ip == request.ip) {
                    intranet = true;
                    domain = row.private_ip.replace(/\./g, '-') + '.cctm.co.kr';
                } else {
                    intranet = false;
                    domain = row.settop_id + '.cctm.co.kr';
                }
                // TEST
                //intranet = true;

                setImmediate(next);
            }, function() {
                if (camera) {
                    fields = ['$1'];
                    values = [camera.id];
                }

                if (!fields.length) {
                    return next(null, {rows: []});
                }

                connection.query(util.format(heredoc(function() {/*
                    SELECT file.*, recorder.id AS recorder_id
                    FROM file, camera, recorder
                    WHERE
                        file.camera_id = camera.id
                        AND
                        camera.recorder_id = recorder.id
                        AND
                        file.camera_id IN (%s)
                        AND
                        start_date BETWEEN '%s' AND '%s'
                        AND
                        file.status = 'download'
                    ORDER BY file.start_date ASC
                */}), fields.join(','), startDate, endDate), values, next);
            });
        },

        function(result, next) {
            files = result.rows;
            files.forEach(function(file) {
                var hours = 0;
                var minutes = 0;
                var seconds = 0;
                var interval = file.end_date - file.start_date;

                interval /= 1000;

                if (interval > 3600) {
                    hours = parseInt(interval / 60 / 60);
                    interval -= 3600 * hours;
                }
                if (interval > 60) {
                    minutes = parseInt(interval / 60);
                    interval -= 60 * minutes;
                }
                seconds = interval;

                if (hours < 10) {
                    hours = '0' + hours;
                }
                if (minutes < 10) {
                    minutes = '0' + minutes;
                }
                if (seconds < 10) {
                    seconds = '0' + seconds;
                }

                file.duration = [hours, minutes, seconds].join(':');
                file.url = 'https://' + domain + '/' + file.recorder_id + '/' + file.camera_id + '/' + file.id + '.mp4';
            });
            next();
        }
    ], function(error) {
        if (done) {
            done();
        }

        if (error) {
            logger.error(error);
            response.redirect('/');
        } else {
            response.render('file/list.html', {now: new Date(), date: date, camera: camera, cameras: cameras, files: files, intranet: intranet});
        }
    });
});

router.get('/file/play', function(request, response, next) {
    var connection = null;
    var done = null;

    var file = null;

    var fileId = request.query.fileId;

    async.waterfall([
        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

            connection.query(heredoc(function() {/*
                SELECT file.id AS id, camera.id AS camera_id, recorder.id AS recorder_id, settop.id AS settop_id, settop.public_ip, settop.private_ip
                FROM file, camera, recorder, settop, client
                WHERE
                    file.camera_id = camera.id
                    AND
                    camera.recorder_id = recorder.id
                    AND
                    recorder.settop_id = settop.id
                    AND
                    settop.client_id = client.id
                    AND
                    client.id = $1
                    AND
                    file.id = $2
                LIMIT 1
            */}), [request.client.id, fileId], next);
        },

        function(result, next) {
            file = result.rows[0];

            var domain = '';
            if (file.public_ip == request.ip) {
                domain = file.private_ip.replace(/\./g, '-') + '.cctm.co.kr';
            } else {
                domain = file.settop_id + '.cctm.co.kr';
            }

            file.url = 'https://' + domain + '/' + file.recorder_id + '/' + file.camera_id + '/' + file.id + '.mp4';

            next();
        }
    ], function(error) {
        if (done) {
            done();
        }

        if (error) {
            logger.error(error);
            response.redirect('/');
        } else {
            response.render('file/play.html', {file: file});
        }
    });
});
