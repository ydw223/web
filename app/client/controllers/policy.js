var pg = require('pg');
var pdf = require('pdf');
var async = require('async');
var config = require('config');
var logger = require('logger');
var express = require('express');
var router = module.exports = express.Router();

router.get('/policy/management.pdf', function(request, response, next) {
    var name = request.client.name + '_영상정보처리기기_내부관리계획.pdf';
    name = encodeURIComponent(name);
    response.set('Content-Disposition', 'attachment; filename=' + name);
    response.set('Content-Type', 'application/pdf');
    pdf.createReadStream('https://' + request.headers.host + '/policy/management?print=1').pipe(response);
});

router.get('/policy/management', function(request, response, next) {
    var connection = null;
    var done = null;

    async.waterfall([
        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

            connection.query('SELECT camera.place, camera.resolution camera_resolution, stream.resolution stream_resolution, stream.frame FROM camera LEFT JOIN stream ON stream.type = \'main\' AND stream.camera_id = camera.id WHERE camera.recorder_id IN (SELECT id FROM recorder WHERE settop_id IN (SELECT id FROM settop WHERE client_id = $1)) ORDER BY channel', [request.client.id], next);
        }
    ], function(error, result) {
        if (done) {
            done();
        }
        if (error) {
            logger.error(error);
            response.redirect('/');
        } else {
            var indexes = [
                '가',
                '나',
                '다',
                '라',
                '마',
                '바',
                '사',
                '아',
                '자',
                '차',
                '카',
            ];
            var places = [
                '보육실',
                '공동놀이실',
                '놀이터',
                '식당',
                '강당',
                '조리실',
                '복도,현관',
                '사무실',
                '양호실',
                '건물외부',
                '기타',
            ];
            var cameras = result.rows;
            var camerasByPlace = {};
            var frame = 0;
            var cameraPixel = 0;
            var streamPixel = 0;
            var cameraResolution = '0x0';
            var streamResolution = '0x0';
            cameras.forEach(function(camera) {
                var place = camera.place || '기타';
                if (!camerasByPlace[place]) {
                    camerasByPlace[place] = [];
                }
                if (camera.camera_resolution) {
                    var tokens = camera.camera_resolution.split('x');
                    var pixel = tokens[0] * tokens[1];
                    if (pixel > cameraPixel) {
                        cameraPixel = pixel;
                        cameraResolution = camera.camera_resolution;
                    }
                }
                if (camera.stream_resolution) {
                    var tokens = camera.stream_resolution.split('x');
                    var pixel = tokens[0] * tokens[1];
                    if (pixel > streamPixel) {
                        streamPixel = pixel;
                        frame = camera.frame;
                        streamResolution = camera.stream_resolution;
                    }
                }
                camerasByPlace[place].push(camera);
            });
            response.render('policy/management.html', {client: request.client, cameras: cameras, camerasByPlace: camerasByPlace, places: places, indexes: indexes, cameraResolution: cameraResolution, streamResolution: streamResolution, frame: frame, print: request.query.print});
        }
    });
});

router.get('/policy/privacy', function(request, response, next) {
});

router.get('/policy/terms', function(request, response, next) {
});
