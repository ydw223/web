var express = require('express');
var app = express();

app.use(require('./controllers/index'));
app.use(require('./controllers/about'));
app.use(require('./controllers/shop'));
app.use(require('./controllers/pamphlet'));
app.use(require('./controllers/faq'));
app.use(require('./controllers/case'));
app.use(require('./controllers/inquiry'));
app.use(require('./controllers/terms'));
app.use(require('./controllers/policy'));
app.use(require('./controllers/browser'));
app.use(function(request, response, next) {
    if (request.path.match(/.html$/)) {
        response.redirect('/');
    } else {
        next();
    }
});
app.use(require('epona'));

module.exports = app;
