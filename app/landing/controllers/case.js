var express = require('express');
var router = module.exports = express.Router();

router.get('/case', function(request, response, next) {
    response.render('case/index.html', {
        controller: 'case'
    });
});
