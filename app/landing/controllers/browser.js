var express = require('express');
var router = module.exports = express.Router();

router.get('/browser', function(request, response, next) {
    response.render('browser/index.html');
});
