var fs = require('fs');
var pg = require('pg');
var net = require('net');
var async = require('async');
var heredoc = require('heredoc');
var config = require('config');
var express = require('express');
var router = module.exports = express.Router();

router.get('/shop', function(request, response, next) {
    response.render('shop/index.html', {
        controller: 'shop'
    });
});

router.get('/shop/done', function(request, response, next) {
    var id = request.query.id;
    if (!id) {
        response.redirect('/');
    }

    var connection = null;
    var done = null;

    async.waterfall([
        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

            connection.query(heredoc(function() {/*
                SELECT *
                FROM service_application
                WHERE id = $1
                LIMIT 1
            */}), [id], next);
        },

        function(result, next) {
            var row = result.rows[0];
            next(null, row);
        }
    ], function(error, result) {
        if (done) {
            done();
        }
        if (error) {
            response.redirect('/');
        } else {
            response.render('shop/done.html', {
                controller: 'shop',
                serviceApplication: result
            });
        }
    });
});

router.post('/shop/submit', function(request, response, next) {
    var serviceApplication = null;
    var connection = null;
    var done = null;

    async.waterfall([
        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

            connection.query('BEGIN');

            var ip = request.headers['x-forwarded-for'] || request.headers['x-real-ip'] || request.connection.remoteAddress || null;
            ip = ip.split(',').shift();
            ip = ip.split(':').shift();
            if (!net.isIP(ip)) {
                ip = null;
            }

            connection.query(heredoc(function() {/*
                INSERT INTO service_application (ip, nursery_name, nursery_address, applicant_name, applicant_email, applicant_phone, ejboo_id)
                VALUES ($1, $2, $3, $4, $5, $6, $7)
                RETURNING id
            */}), [ip, request.body.nurseryName, request.body.nurseryAddress, request.body.applicantName, request.body.applicantEmail, request.body.applicantPhone, request.body.ejbooId], next);
        },

        function(result, next) {
            serviceApplication = result.rows[0];

            connection.query(heredoc(function() {/*
                SELECT *
                FROM service_application
                WHERE id = $1
                LIMIT 1
            */}), [serviceApplication.id], next);
        },

        function(result, next) {
            serviceApplication = result.rows[0];

            var subject = {
                nurseryName: request.body.nurseryName
            };

            var content = {
                number: serviceApplication.number,
                nurseryName: request.body.nurseryName,
                nurseryAddress: request.body.nurseryAddress,
                applicantName: request.body.applicantName,
                applicantEmail: request.body.applicantEmail,
                applicantPhone: request.body.applicantPhone,
                ejbooId: request.body.ejbooId
            };

            connection.query(heredoc(function() {/*
                INSERT INTO envelope (letter_id, type, "from", "to", bcc, subject, content)
                VALUES ((SELECT id FROM letter WHERE key = 'service_application' LIMIT 1), $1, $2, $3, $4, $5, $6)
            */}), ['email', 'CCTM <admin@cctm.kr>', request.body.applicantName + ' <' + request.body.applicantEmail + '>', request.body.applicantName + ' <admin@cctm.kr>', subject, content], next);
        },

        function(result, next) {
            connection.query('COMMIT', next);
        },

        function(result, next) {
            next(null, serviceApplication);
        }
    ], function(error, result) {
        if (done) {
            done();
        }
        if (error) {
            console.log(error.stack);
            response.status(500).json(error);
        } else {
            response.json(result);
        }
    });
});

router.get('/shop/nursery', function(request, response, next) {
    var connection = null;
    var done = null;

    async.waterfall([
        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

//            var cho = ['ㄱ','ㄲ','ㄴ','ㄷ','ㄸ','ㄹ','ㅁ','ㅂ','ㅃ','ㅅ','ㅆ','ㅇ','ㅈ','ㅉ','ㅊ','ㅋ','ㅌ','ㅍ','ㅎ'];
//            var search = request.query.search.replace(new RegExp('$' + cho.join('|'), 'g'), '');
//            if (search.length > 0) {
//                connection.query(heredoc(function() {/*
//                    --SELECT name, substr(address, 11) AS address
//                    SELECT name, address
//                    FROM nursery
//                    WHERE name LIKE '%' || $1 || '%' and status = 'done'
//                    --ORDER BY name, substr(address, 11)
//                    ORDER BY name, address
//                    LIMIT 50
//                */}), [search], next);
//            } else {
//                next(null, {rows: []});
//            }

            connection.query(heredoc(function() {/*
                --SELECT name, substr(address, 11) AS address
                SELECT name, address
                FROM nursery
                WHERE name ILIKE '%' || $1 || '%' and status = 'done'
                --ORDER BY name, substr(address, 11)
                ORDER BY name, address
                LIMIT 100
            */}), [request.query.search], next);
        },

        function(result, next) {
            async.mapSeries(result.rows, function(row, next) {
                next(null, row);
            }, next)
        }
    ], function(error, result) {
        if (done) {
            done();
        }
        if (error) {
            console.log(error.stack);
            response.status(500).json(error);
        } else {
            response.json(result);
        }
    });
});
