var express = require('express');
var router = module.exports = express.Router();

router.get('/pamphlet', function(request, response, next) {
    response.render('pamphlet/index.html', {
        controller: 'pamphlet'
    });
});
