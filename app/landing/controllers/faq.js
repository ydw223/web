var express = require('express');
var router = module.exports = express.Router();

router.get('/faq', function(request, response, next) {
    response.render('faq/index.html', {
        controller: 'faq'
    });
});
