var express = require('express');
var router = module.exports = express.Router();

router.get('/about/summary', function(request, response, next) {
    response.render('about/summary.html', {
        controller: 'about'
    });
});

router.get('/about/contact', function(request, response, next) {
    response.render('about/contact.html', {
        controller: 'about'
    });
});
