var express = require('express');
var router = module.exports = express.Router();

router.get('/inquiry', function(request, response, next) {
    response.render('inquiry/index.html', {
        controller: 'inquiry'
    });
});
