qx.Theme.define('cctm.theme.Font', {
    extend: qx.theme.indigo.Font,

    fonts: {
        'default': {
            size: 12,
            family: ['NanumGothic-Regular'],
            sources: [{
                family: 'NanumGothic-Regular',
                source: [
                    '/fonts/NanumGothic-Regular.eot',
                    '/fonts/NanumGothic-Regular.ttf',
                    '/fonts/NanumGothic-Regular.woff',
                    '/fonts/NanumGothic-Regular.svg#NanumGothic-Regular'
                ]
            }]
        },

        bold: {
            size: 12,
            family: ['NanumGothic-Bold'],
            sources: [{
                family: 'NanumGothic-Bold',
                source: [
                    '/fonts/NanumGothic-Bold.eot',
                    '/fonts/NanumGothic-Bold.ttf',
                    '/fonts/NanumGothic-Bold.woff',
                    '/fonts/NanumGothic-Bold.svg#NanumGothic-Bold'
                ]
            }]
        },

        large: {
            size: 14,
            family: ['NanumGothic-Regular'],
            sources: [{
                family: 'NanumGothic-Regular',
                source: [
                    '/fonts/NanumGothic-Regular.eot',
                    '/fonts/NanumGothic-Regular.ttf',
                    '/fonts/NanumGothic-Regular.woff',
                    '/fonts/NanumGothic-Regular.svg#NanumGothic-Regular'
                ]
            }]
        },

        xlarge: {
            size: 20,
            family: ['NanumGothic-Bold'],
            sources: [{
                family: 'NanumGothic-Bold',
                source: [
                    '/fonts/NanumGothic-Bold.eot',
                    '/fonts/NanumGothic-Bold.ttf',
                    '/fonts/NanumGothic-Bold.woff',
                    '/fonts/NanumGothic-Bold.svg#NanumGothic-Bold'
                ]
            }]
        }
    }
});
