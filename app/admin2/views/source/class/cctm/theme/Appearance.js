qx.Theme.define('cctm.theme.Appearance', {
    extend: qx.theme.indigo.Appearance,

    appearances: {
        'login': {
            style: function(states, styles) {
                return {
                };
            }
        },

        'main': {
            style: function(states, styles) {
                return {
                };
            }
        },

        'main/top': {
            style: function(states, styles) {
                return {
                    height: 45,
                    padding: 5,
                    textColor: '#ffffff',
                    decorator: 'top-background'
                };
            }
        },

        'main/top/logo': {
            style: function(states) {
                return {
                    width: 150,
                    font: 'xlarge',
                    textAlign: 'center'
                };
            }
        },

        'main/top/menu': {
            style: function(states) {
                return {
                    font: 'large'
                };
            }
        },

        'main/top/extra': {
            style: function(states) {
                return {
                    paddingRight: 10
                };
            }
        },

        'main/middle': {
            style: function(states, styles) {
                return {
                    padding: 10
                };
            }
        },

        'main/middle/loading': {
            style: function(states) {
                return {
                    decorator: 'loading-background'
                };
            }
        },

        'main/middle/list/table': {
            alias: 'table',
            include: 'table'
        },

        'table-scroller/pane': {
            style: function(states, styles) {
                return {
                    decorator: 'table-scroller-pane'
                };
            }
        },

        'table-scroller/scrollbar-y': {
            alias: 'scrollbar',
            include: 'scrollbar',

            style: function(states, styles) {
                return {
                    decorator: 'table-scrollbar-y'
                };
            }
        }
    }
});
