qx.Theme.define('cctm.theme.Decoration', {
    extend: qx.theme.indigo.Decoration,

    decorations: {
        'top-background': {
            style: {
                widthBottom: 3,
                color: '#3D72C9',
                gradientStart: ['#505154', 30],
                gradientEnd: ['#323335', 100]
            }
        },

        'loading-background': {
            style: {
                backgroundImage: 'resource/cctm/loading.gif',
                backgroundPositionX: 'center',
                backgroundPositionY: 'middle',
                backgroundRepeat: 'no-repeat'
            }
        },

        'table-header': {
            include: 'button-box',

            style: {
                radius: 0,
                width: [1, 1, 1, 0]
            }
        },

        'table-scroller-pane': {
            style: {
                widthBottom: 1,
                widthLeft: 1,
                widthRight: 1,
                color: 'border-main'
            }
        },

        'table-scrollbar-y': {
            style: {
                widthRight: 1,
                widthBottom: 1,
                color: 'border-main'
            }
        },

        'separator-dot': {
            style: {
                widthLeft: 1,
                styleLeft: 'dotted',
                colorLeft: '#cccccc'
            }
        }
    }
});
