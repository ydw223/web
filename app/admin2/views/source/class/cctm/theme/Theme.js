qx.Theme.define('cctm.theme.Theme', {
    meta: {
        color: cctm.theme.Color,
        decoration: cctm.theme.Decoration,
        font: cctm.theme.Font,
        icon: qx.theme.icon.Tango,
        appearance: cctm.theme.Appearance
    }
});
