qx.Class.define('cctm.application.Login', {
    extend: qx.ui.container.Composite,

    construct: function() {
        var layout = new qx.ui.layout.HBox();
        this.setLayout(layout);

        this.base(arguments, layout);

        this.add(new qx.ui.form.Button('login'));
    },

    members: {
        _createChildControlImpl: function(id) {
            var control;
            switch (id) {
            }
            return control || this.base(arguments, id);
        }
    }
});
