qx.Class.define('cctm.application.main.middle.Loading', {
    extend: qx.ui.container.Composite,

    construct: function() {
        this.base(arguments, new qx.ui.layout.HBox());
    }
});
