qx.Class.define('cctm.application.main.Top', {
    extend: qx.ui.container.Composite,

    construct: function() {
        var layout = new qx.ui.layout.HBox();
        layout.setAlignY('middle');

        this.base(arguments, layout);

        this._createChildControl('logo');
        this._createChildControl('menu');
        this._createChildControl('extra');
    },

    members: {
        _createChildControlImpl: function(id) {
            var control;
            switch (id) {
                case 'logo':
                    control = new cctm.application.main.top.Logo();
                    this.add(control);
                    break;
                case 'menu':
                    control = new cctm.application.main.top.Menu();
                    this.add(control, {flex: 1});
                    break;
                case 'extra':
                    control = new cctm.application.main.top.Extra();
                    this.add(control, {flex: 1});
                    break;

            }
            return control || this.base(arguments, id);
        }
    }
});
