qx.Class.define('cctm.application.main.top.Logo', {
    extend: qx.ui.basic.Label,

    construct: function() {
        this.base(arguments, 'CCTM');
    }
});
