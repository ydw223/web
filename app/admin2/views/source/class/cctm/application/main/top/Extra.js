qx.Class.define('cctm.application.main.top.Extra', {
    extend: qx.ui.container.Composite,

    construct: function() {
        var layout = new qx.ui.layout.HBox(6);
        layout.setAlignX('right');
        layout.setAlignY('middle');

        this.base(arguments);
        this.setLayout(layout);
        this.setAllowGrowY(false);

        this._createChildControl('logout');
    },

    members: {
        _createChildControlImpl: function(id) {
            var control;
            switch (id) {
                case 'logout':
                    control = new cctm.ui.form.LabelButton('로그아웃');
                    control.addListener('execute', this.__onLogout, this);
                    break;
            }
            if (control) {
                this.add(control);
            }
            return control || this.base(arguments, id);
        },

        __onLogout: function(e) {
            qx.bom.Cookie.del('sessionId');
            qx.event.message.Bus.dispatchtop('logout');
        }
    },

    destruct: function() {
        this.getChildControl('logout').removeListener('execute', this.__onLogout, this);
    }
});
