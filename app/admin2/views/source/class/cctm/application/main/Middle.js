qx.Class.define('cctm.application.main.Middle', {
    extend: qx.ui.container.Stack,

    construct: function() {
        this.base(arguments);
        this.setDynamic(true);

        qx.event.message.Bus.subscribe('changeMenu', this.__onChangeMenu, this);
    },

    members: {
        __loadedParts: {},

        _createChildControlImpl: function(id) {
            var control;
            switch (id) {
                case 'loading':
                    control = new cctm.application.main.middle.Loading();
                    this.add(control);
                    break;
                case 'client':
                    control = new cctm.application.main.middle.Client();
                    break;
            }
            if (control) {
                this.add(control);
            }
            return control || this.base(arguments, id);
        },

        __onChangeMenu: function(e) {
            var id = e.getData().id;
            var partLoader = qx.io.PartLoader.getInstance();

            if (this.__loadedParts[id] || !partLoader.hasPart(id)) {
                this.setSelection([this.getChildControl(id)]);
            } else {
                this.setSelection([this.getChildControl('loading')]);
                partLoader.require([id], function() {
                    this.__loadedParts[id] = true;
                    this.setSelection([this.getChildControl(id)]);
                }, this);
            }
        }
    },

    destruct: function() {
        qx.event.message.Bus.unsubscribe('changeMenu', this.__onChangeMenu, this);
        this.__loadedParts = null;
    }
});
