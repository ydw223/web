qx.Class.define('cctm.application.main.top.Menu', {
    extend: qx.ui.container.Composite,

    construct: function() {
        var layout = new qx.ui.layout.HBox(20);
        layout.setAlignX('left');
        layout.setAlignY('middle');

        this.base(arguments);
        this.setLayout(layout);
        this.setAllowGrowY(false);

        for (var menu in this.__menus) {
            this._createChildControl(menu);
        }
    },

    members: {
        __menus: {
            'client': '어린이집 관리',
            'service': '서비스 신청 관리'
        },

        _createChildControlImpl: function(id) {
            var control;
            var menu = this.__menus[id];
            if (menu) {
                control = new cctm.ui.form.LabelButton(menu);
                control.setUserData('model', id);
                control.addListener('execute', this.__onChangeMenu, this);
                this.add(control);

                if (id != 'service') {
                    this.add(new qx.ui.core.Widget().set({
                        maxHeight: 5,
                        maxWidth: 1,
                        decorator: 'separator-dot'
                    }));
                }
            }
            return control || this.base(arguments, id);
        },

        __onChangeMenu: function(e) {
            var target = e.getTarget();
            this.__dispatchChangeMenu(target.getUserData('model'));
        },

        __dispatchChangeMenu: function(menu) {
            qx.event.message.Bus.dispatchByName('changeMenu', {id: menu});
        }
    },

    destruct: function() {
        for (var menu in this.__menus) {
            this.getChildControl(menu).removeListener('execute', this.__onDns, this);
        }
        this.__menus = null;
    }
});
