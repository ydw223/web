qx.Class.define('cctm.application.Main', {
    extend: qx.ui.container.Composite,

    construct: function() {
        var layout = new qx.ui.layout.VBox();
        this.setLayout(layout);

        this.base(arguments, layout);

        this._createChildControl('top');
        this._createChildControl('middle');
        this._createChildControl('bottom');
    },

    members: {
        _createChildControlImpl: function(id) {
            var control;
            switch (id) {
                case 'top':
                    control = new cctm.application.main.Top();
                    this.add(control);
                    break;
                case 'middle':
                    control = new cctm.application.main.Middle();
                    this.add(control, {flex: 1});
                    break;
                case 'bottom':
                    control = new cctm.application.main.Bottom();
                    this.add(control);
                    break;
            }
            return control || this.base(arguments, id);
        }
    }
});
