qx.Class.define('cctm.Application', {
    extend: qx.application.Standalone,

    members: {
        main: function() {
            this.base(arguments);

            if (qx.core.Environment.get('qx.debug')) {
                qx.log.appender.Native;
                qx.log.appender.Console;
            }

            Date.fromISO = function(value) {
                var a = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
                if (a) {
                    return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4], +a[5], +a[6]));
                }
                return null;
            };

            this.selectApplication('main');
            window.setProgress(100);

            qx.event.message.Bus.subscribe('login', this.__onLogin, this);
            qx.event.message.Bus.subscribe('logout', this.__onLogout, this);
        },

        selectApplication: function(id) {
            qx.io.PartLoader.require([id], function() {
                this.__container.setSelection([this.__getApplication(id)]);

                if (!this.getRoot().isVisible()) {
                    this.getRoot().show();

                    var element = window.document.getElementById('loading');
                    element.parentNode.removeChild(element);
                }
            }, this);
        },

        __getApplication: function(id) {
            if (!this.__applications[id]) {
                var application;
                switch (id) {
                    case 'login':
                        application = new cctm.application.Login();
                        break;
                    case 'main':
                        application = new cctm.application.Main();
                        break;
                }
                if (application) {
                    application.setAppearance(id);
                    this.__container.add(application);
                    this.__applications[id] = application;
                }
            }
            return this.__applications[id];
        },

        _createRootWidget: function() {
            window.setProgress(80);

            var root = this.base(arguments);
            root.exclude();

            this.__container = new qx.ui.container.Stack();
            this.__container.setDynamic(true);
            this.__applications = {};

            var scroll = new qx.ui.container.Scroll();
            scroll.add(this.__container, {flex: 1});
            root.add(scroll, {edge: 0});

            return root;
        },

        __onLogin: function(e) {
            this.selectApplication('main');
        },

        __onLogout: function(e) {
            this.selectApplication('login');
        }
    }
});
