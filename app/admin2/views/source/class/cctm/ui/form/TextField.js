qx.Class.define('cctm.ui.form.TextField', {
    extend: qx.ui.form.TextField,

    properties: {
        nativeContextMenu: {
            refine: true,
            init: true
        }
    }
});
