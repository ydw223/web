qx.Class.define('cctm.ui.form.PasswordField', {
    extend: qx.ui.form.PasswordField,

    properties: {
        nativeContextMenu: {
            refine: true,
            init: true
        }
    }
});
