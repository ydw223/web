qx.Class.define('cctm.ui.form.LabelButton', {
    extend: qx.ui.basic.Label,

    events: {
        execute: 'qx.event.type.Event'
    },

    properties: {
        showUnderline: {
            init: false,
            apply : '_applyShowUnderline'
        },
        showLinkColor: {
            init: false,
            apply : '_applyShowLinkColor'
        },
        buttonMode: {
            apply : '_applyButtonMode'
        }
    },

    construct: function(value) {
        this.base(arguments, value);
        this.setButtonMode(true);
    },

    members: {
        syncAppearance: function() {
            this.base(arguments);
            if (this.getShowUnderline()) {
                this.getContentElement().setStyle('textDecoration', 'underline');
            } else {
                this.getContentElement().setStyle('textDecoration', null);
            }
        },

        _applyShowUnderline: function(value, old) {
        },

        _applyShowLinkColor: function(value, old) {
            if (value) {
                this.setTextColor('text-link');
            } else {
                this.resetTextColor();
            }
        },

        _applyButtonMode: function(value, old) {
            this.setCursor('default');
            this.getContentElement().setStyle('textDecoration', null);
            this.removeListener('mouseover', this._onMouseOver, this);
            this.removeListener('mouseout', this._onMouseOut, this);
            this.removeListener('click', this._onClick, this);
            if (value) {
                this.setCursor('pointer');
                this.addListener('mouseover', this._onMouseOver, this);
                this.addListener('mouseout', this._onMouseOut, this);
                this.addListener('click', this._onClick, this);
            }
        },

        _onMouseOver: function(e) {
            this.getContentElement().setStyle('textDecoration', 'underline');
        },

        _onMouseOut: function(e) {
            if (!this.getShowUnderline()) {
                this.getContentElement().setStyle('textDecoration', null);
            }
        },
        
        _onClick: function(e) {
            this.fireEvent('execute');
        }
    }
});
