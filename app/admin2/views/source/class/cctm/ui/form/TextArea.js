qx.Class.define('cctm.ui.form.TextArea', {
    extend: qx.ui.form.TextArea,

    properties: {
        nativeContextMenu: {
            refine: true,
            init: true
        }
    }
});
