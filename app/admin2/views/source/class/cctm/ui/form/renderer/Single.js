qx.Class.define('cctm.ui.form.renderer.Single', {
    extend: qx.ui.form.renderer.Single,

    construct: function(form) {
        this.base(arguments, form);
        this.getLayout().setColumnFlex(0, 0);
        this.getLayout().setColumnFlex(1, 1);
    },

    members: {
        addItems: function(items, names, title, options) {
            if (title != null) {
                this._add(this._createHeader(title), {row: this._row, column: 0, colSpan: 2});
                this._row++;
            }

            for (var i = 0; i < items.length; i++) {
                var label = this._createLabel(names[i], items[i]);
                this._add(label, {row: this._row, column: 0});
                var item = items[i];
                label.setBuddy(item);
                this._add(item, {row: this._row, column: 1});
                this._row++;

                this._connectVisibility(item, label);

                if (qx.core.Environment.get('qx.dynlocale')) {
                    this._names.push({name: names[i], label: label, item: items[i]});
                }

                if (options[i] && options[i].message) {
                    var label = new qx.ui.basic.Label(options[i].message).set({rich: true});
                    label.setBuddy(item);

                    this._add(label, {row: this._row, column: 1});
                    this._row++;
                }
            }
        },

        _createLabelText: function(name, item) {
            return name;
        }
    }
});
