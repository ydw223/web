qx.Class.define('cctm.ui.table.cellrenderer.Number', {
    extend: qx.ui.table.cellrenderer.Number,

    construct: function() {
        this.base(arguments);
        this.setNumberFormat(new qx.util.format.NumberFormat());
    }
});
