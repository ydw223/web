qx.Class.define('cctm.ui.table.celleditor.TextField', {
    extend: qx.ui.table.celleditor.TextField,

    members: {
        __table: null,

        createCellEditor: function(cellInfo) {
            this.__table = cellInfo.table;
            return this.base(arguments, cellInfo);
        },

        _createEditor: function() {
            var cellEditor = new cctm.ui.form.TextField();
            cellEditor.setAppearance('table-editor-textfield');
            cellEditor.addListener('keypress', function(e) {
                if (!e.isCtrlPressed()) {
                    this.__table.handleKeyPress(e);
                }
            }, this);
            cellEditor.addListener('focus', function(e) {
                this.selectAllText();
            });
            this.__cellEditor = cellEditor;
            return cellEditor;
        }
    }
});
