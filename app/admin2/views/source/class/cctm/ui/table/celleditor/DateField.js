qx.Class.define('cctm.ui.table.celleditor.DateField', {
    extend: cctm.ui.table.celleditor.TextField,

    members: {
        __cellInfo: null,

        getCellEditorValue: function(cellEditor) {
            var value = new Date(cellEditor.getValue());
            var old = new Date(this.__cellInfo.value);
            return new Date(value.getFullYear(), value.getMonth(), value.getDate(), old.getHours(), old.getMinutes(), old.getSeconds(), old.getMilliseconds());
        },

        createCellEditor: function(cellInfo) {
            this.__cellInfo = cellInfo;
            var cellEditor = this.base(arguments, cellInfo);
            var date = new Date(cellInfo.value);
            var month = date.getMonth() + 1;
            if (month < 10) {
                month = '0' + month;
            }
            var day = date.getDate();
            if (day < 10) {
                day = '0' + day;
            }
            cellEditor.setValue(date.getFullYear() + '-' + month + '-' + day);
            return cellEditor;
        }
    }
});
