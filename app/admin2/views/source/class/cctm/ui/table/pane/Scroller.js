qx.Class.define('cctm.ui.table.pane.Scroller', {
    extend: qx.ui.table.pane.Scroller,

    properties: {
        selectBeforeFocus: {
            refine: true,
            init: true
        }
    },

    members: {
        _onMousemoveHeader: function(e) {
        }
    }
});
