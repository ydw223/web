qx.Class.define('cctm.ui.table.celleditor.NumberField', {
    extend: cctm.ui.table.celleditor.TextField,

    members: {
        getCellEditorValue: function(cellEditor) {
            var value = parseInt(cellEditor.getValue().replace(',', ''));
            if (!value) {
                value = 0;
            }
            if (isNaN(value)) {
                cellEditor.setValue(cellEditor.originalValue + '');
            } else {
                cellEditor.setValue(value + '');
            }
            return this.base(arguments, cellEditor);
        }
    }
});
