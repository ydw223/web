qx.Class.define('cctm.ui.table.model.Remote', {
    extend: qx.ui.table.model.Remote,

    properties: {
        conditions: {
            nullable: true
        }
    },

    members: {
        getValue: function(columnIndex, rowIndex) {
            var rowData = this.getRowData(rowIndex);
            if (rowData == null) {
                return null;
            } else {
                var columnId = this.getColumnId(columnIndex);
                var columnIds = columnId.split('.');
                for (var i = 0; i < columnIds.length; i++) {
                    if (rowData) {
                        rowData = rowData[columnIds[i]];
                    }
                }
                if (rowData === 0) {
                    rowData = '0';
                }
                return String(rowData || '');
            }
        },

        setValue: function(columnIndex, rowIndex, value) {
            var rowData = this.getRowData(rowIndex);
            if (rowData == null) {
                return;
            } else {
                var columnId = this.getColumnId(columnIndex);
                var columnIds = columnId.split('.');
                for (var i = 0; i < columnIds.length - 1; i++) {
                    rowData = rowData[columnIds[i]];
                }
                rowData[columnIds[columnIds.length - 1]] = value;

                if (this.hasListener('dataChanged')) {
                    var data = {
                        firstRow    : rowIndex,
                        lastRow     : rowIndex,
                        firstColumn : columnIndex,
                        lastColumn  : columnIndex
                    };
                    this.fireDataEvent('dataChanged', data);
                }
            }
        }
    }
});
