qx.Class.define('cctm.ui.table.celleditor.TimeField', {
    extend: cctm.ui.table.celleditor.TextField,

    members: {
        __cellInfo: null,

        getCellEditorValue: function(cellEditor) {
            var value = new Date('1970-01-01 ' + cellEditor.getValue());
            var old = new Date(this.__cellInfo.value);
            return new Date(old.getFullYear(), old.getMonth(), old.getDate(), value.getHours(), value.getMinutes(), value.getSeconds(), old.getMilliseconds());
        },

        createCellEditor: function(cellInfo) {
            this.__cellInfo = cellInfo;
            var cellEditor = this.base(arguments, cellInfo);
            var date = new Date(cellInfo.value);
            var hour = date.getHours();
            if (hour < 10) {
                hour = '0' + hour;
            }
            var minute = date.getMinutes();
            if (minute < 10) {
                minute = '0' + minute;
            }
            var second = date.getSeconds();
            if (second < 10) {
                second = '0' + second;
            }
            cellEditor.setValue(hour + ':' + minute + ':' + second);
            return cellEditor;
        }
    }
});
