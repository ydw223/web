qx.Class.define('cctm.ui.table.cellrenderer.Date', {
    extend: qx.ui.table.cellrenderer.Date,

    construct: function() {
        this.base(arguments);
        this.setDateFormat(new qx.util.format.DateFormat('YYYY-MM-dd'));
    },

    members: {
        _getContentHtml: function(cellInfo) {
            if (!cellInfo.value) {
                return '';
            }
            return this.getDateFormat().format(Date.fromISO(cellInfo.value));
        }
    }
});

