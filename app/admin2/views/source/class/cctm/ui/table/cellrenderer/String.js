qx.Class.define('cctm.ui.table.cellrenderer.String', {
    extend: qx.ui.table.cellrenderer.String,

    members: {
        _getCellStyle: function(cellInfo) {
            return qx.bom.element.Style.compile({
                textAlign: 'center'
            });
        }
    }
});
