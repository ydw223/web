qx.Class.define('cctm.ui.table.cellrenderer.Time', {
    extend: qx.ui.table.cellrenderer.Date,

    construct: function() {
        this.base(arguments);
        this.setDateFormat(new qx.util.format.DateFormat('HH:mm:ss'));
    },

    members: {
        _getContentHtml: function(cellInfo) {
            if (!cellInfo.value) {
                return '';
            }
            return this.getDateFormat().format(new Date(cellInfo.value));
        }
    }
});

