qx.Class.define('cctm.ui.table.Table', {
    extend: qx.ui.table.Table,

    include: [
        qx.ui.table.MTableContextMenu
    ],

    construct: function(tableModel, factoryMethod) {
        qx.ui.table.columnmodel.Basic.DEFAULT_DATA_RENDERER = cctm.ui.table.cellrenderer.Default;

        if (!tableModel) {
            tableModel = new qx.ui.table.model.Simple();
        }

        if (!factoryMethod) {
            factoryMethod = {};
        }

        if (!factoryMethod.tableColumnModel) {
            factoryMethod.tableColumnModel = function(table) {
                return new cctm.ui.table.columnmodel.Resize(table);
            }
        }

        if (!factoryMethod.tablePaneScroller) {
            factoryMethod.tablePaneScroller = function(table) {
                return new cctm.ui.table.pane.Scroller(table);
            }
        }
        this.base(arguments, tableModel, factoryMethod);

        this.setRowHeight(30);
        this.setStatusBarVisible(false);
        this.setColumnVisibilityButtonVisible(false);
        this.setKeepFirstVisibleRowComplete(true);
        this.setShowCellFocusIndicator(false);

        var selectionModel = this.getSelectionModel();
        selectionModel.setSelectionMode(qx.ui.table.selection.Model.MULTIPLE_INTERVAL_SELECTION);
        selectionModel.addListener('changeSelection', function(e) {
            var selection = [];
            var tableModel = this.getTableModel();

            e.getTarget().iterateSelection(function(i) {
                selection.push(tableModel.getRowData(i));
            });

            this.setSelection(selection);
            this.fireDataEvent('selection', selection);
        }, this);
        this.setSelection([]);

        this.addListener('appear', function() {
            //this.refresh();
        }, this);

        this.addListener('disappear', function() {
        }, this);
    },

    properties: {
        selection: {
            nullable: false
        }
    },

    events: {
        selection: 'qx.event.type.Data'
    },

    members: {
        refresh: function() {
            this.getTableModel().reloadData();
        }
    }
});
