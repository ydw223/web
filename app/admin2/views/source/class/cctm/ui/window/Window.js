qx.Class.define('cctm.ui.window.Window', {
    extend: qx.ui.window.Window,

    construct: function(caption, icon) {
        this.base(arguments, caption, icon);

        this.setAllowMaximize(false);
        this.setAllowMinimize(false);
        this.setShowMaximize(false);
        this.setShowMinimize(false);
        this.setResizable(false);
        this.setMovable(true);
        this.setModal(true);
    }
});
