qx.Class.define('cctm.ui.window.Form', {
    extend: cctm.ui.window.Window,

    type: 'abstract',

    construct: function(caption, icon) {
        var layout = new qx.ui.layout.VBox(10);

        this.__form = new qx.ui.form.Form();
        this.__label = new qx.ui.basic.Label();
        /*
        this.__label.setDecorator('error-background');
        */
        this.__label.setTextColor('red');
        this.__label.setRich(true);
        this.__label.setAllowGrowX(true);
        this.__label.setPadding(10, 5);
        this.__label.exclude();

        var button = new cctm.ui.form.Button('확인');
        button.addListener('execute', this._onConfirm, this);
        this.__form.addButton(button);

        var button = new cctm.ui.form.Button('취소');
        button.addListener('execute', this._onCancel, this);
        this.__form.addButton(button);

        this.base(arguments, caption, icon);
        this.setLayout(layout);
        this.add(this.__label);
        this.addListener('appear', this._onAppear, this);
        this.addListener('disappear', this._onDisappear, this);
    },

    properties: {
        /*
        model: {
            nullable: true,
            apply: '_applyModel'
        }
        */
    },

    events: {
        complete: 'qx.event.type.Event'
    },

    members: {
        __render: false,
        __form: null,
        __model: null,
        __label: null,

        open: function() {
            if (!this.__render) {
                this.render();
            }
            this.base(arguments);
        },

        render: function() {
            this.__render = true;
            this.add(new cctm.ui.form.renderer.Single(this.__form));
        },

        getForm: function() {
            return this.__form;
        },

        getModel: function() {
            if (!this.__model) {
                this.__model = {};
            }

            var form = this.getForm();
            var items = form.getItems();

            for (var name in items) {
                var value = null;
                var item = items[name];

                if (qx.Class.hasInterface(item.constructor, qx.ui.core.ISingleSelection)) {
                    value = item.getModelSelection().getItem(0) || null; 
                } else {
                    value = item.getValue();
                }

                if (name.indexOf('.') >= 0) {
                    var model = this.__model;
                    var tokens = name.split('.');

                    for (var i = 0, length = tokens.length - 1; i < length; i++) {
                        var token = tokens[i];
                        if (!model[token]) {
                            model[token] = {};
                        }
                        model = model[token];
                    }
                    model[tokens[tokens.length - 1]] = value;
                } else {
                    this.__model[name] = value;
                }
            }

            return this.__model;
        },

        setModel: function(model) {
            var form = this.getForm();

            if (model) {
                var items = form.getItems();
                for (var name in items) {
                    var value = null;
                    var item = items[name];

                    if (name.indexOf('.') >= 0) {
                        var __model = model;
                        var tokens = name.split('.');

                        for (var i = 0, length = tokens.length - 1; i < length; i++) {
                            var token = tokens[i];
                            __model = __model[token];
                            if (!__model) {
                                break;
                            }
                        }
                        if (__model) {
                            value = __model[tokens[tokens.length - 1]];
                            if (value === undefined) {
                                value = null;
                            } else if ('number' == typeof value) {
                                value = String(value);
                            }
                        }
                    } else {
                        value = model[name];
                        if (value === undefined) {
                            value = null;
                        }
                    }

                    if (qx.Class.hasInterface(item.constructor, qx.ui.core.ISingleSelection)) {
                        var children = item.getChildren();
                        for (var i = 0, length = children.length; i < length; i++) {
                            var child = children[i];
                            if (child.getModel() == value) {
                                item.setSelection([child]);
                                break;
                            }
                        }
                    } else {
                        if ('number' == typeof value) {
                            value = String(value);
                        }
                        item.setValue(value);
                    }
                }
            } else {
                this.resetModel();
            }

            this.__model = model;
        },

        resetModel: function() {
            this.__form.reset();
            this.__model = null;
        },

        _onSuccess: function(result) {
            this.fireEvent('complete');
            this.close();
            this.__label.exclude();
            this.__label.resetValue();
        },

        _onFailure: function(message) {
            this.__label.setValue(message);
            this.__label.show();
        },

        _onAppear: function(e) {
            this.center();
        },

        _onDisappear: function(e) {
            this.__label.exclude();
            this.__form.reset();
        },

        _onConfirm: function(e) {
            this.close();
        },

        _onCancel: function(e) {
            this.close();
        }
    }
});
