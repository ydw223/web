var fs = require('fs');
var express = require('express');
var session = require('express-session')
var RedisStore = require('connect-redis')(session);
var app = express();

app.use(session({
    store: new RedisStore({ttl: 86400}),
    secret: 'ejboocctm',
    resave: false,
    saveUninitialized: true
}));

if (fs.existsSync(__dirname + '/views/build')) {
    app.use('/', express.static(__dirname + '/views/build'));
} else {
    app.use('/', express.static(__dirname + '/../../'));
}

module.exports = app;
