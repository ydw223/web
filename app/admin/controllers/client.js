var pg = require('pg');
var uuid = require('uuid');
var async = require('async');
var config = require('config');
var logger = require('logger');
var express = require('express');
var router = module.exports = express.Router();

router.get('/client/list', function(request, response, next) {
    var connection = null;
    var done = null;
    var clients = null;

    async.waterfall([
        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

            connection.query('SELECT c.id, c.name, c.representative, c.telephone, c.cellphone, c.note, u.email, s.id AS settop_id, s.public_ip, s.online, s.lock, s.pending, s.online_date, s.offline_date, s.version AS settop_version, r.id AS recorder_id, r.version AS recorder_version, r.firmware AS firmware_version, d.name AS domain_name FROM client c, client_user cu, "user" u, settop s, recorder r, domain d WHERE c.id = cu.client_id AND u.id = cu.user_id ANd c.id = s.client_id AND s.id = r.settop_id and d.client_id = c.id AND cu.type = \'owner\' ORDER BY s.create_date DESC', next);
        },

        function(result, next) {
            clients = result.rows;
            next();
        }
    ], function(error) {
        if (done) {
            done();
        }
        if (error) {
            logger.error(error);
            response.redirect('/');
        } else {
            response.render('client/list.html', {
                controller: 'client',
                clients: clients
            });
        }
    });
});

router.get('/client/edit', function(request, response, next) {
    var connection = null;
    var done = null;
    var client = null;
    var cameras = [];

    var id = request.query.id;

    async.waterfall([
        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

            if (id) {
                connection.query('SELECT c.id, c.name, c.representative, c.telephone, c.cellphone, c.note, c.option, c.monitor, c.place, u.email, d.name AS domain_name, s.id AS settop_id, s.ip, s.private_ip, s.public_ip, s.online, s.lock, s.pending, s.device1, s.device2, r.ip AS recorder_ip, (SELECT count(id) FROM camera WHERE recorder_id IN (SELECT id FROM recorder WHERE settop_id = s.id)) AS camera_count FROM client c, client_user cu, "user" u, settop s, recorder r, domain d WHERE c.id = cu.client_id AND u.id = cu.user_id ANd c.id = s.client_id AND s.id = r.settop_id AND c.id = d.client_id AND cu.type = \'owner\' AND c.id = $1 LIMIT 1', [id], next);
            } else {
                next(null, {rows: []});
            }
        },

        function(result, next) {
            client = result.rows[0];
            if (client) {
                connection.query('SELECT camera.id, camera.name, camera.resolution camera_resolution, camera.place, stream.* FROM camera LEFT JOIN stream ON stream.type = \'main\' AND stream.camera_id = camera.id WHERE camera.recorder_id IN (SELECT id FROM recorder WHERE settop_id IN (SELECT id FROM settop WHERE client_id = $1)) ORDER BY channel', [id], next);
            } else {
                next(null, {rows: []});
            }
        },

        function(result, next) {
            cameras = result.rows;
            connection.query('SELECT * FROM settop WHERE status = \'register\'', next);
        }
    ], function(error, result) {
        if (done) {
            done();
        }
        if (error) {
            logger.error(error);
            response.redirect('/');
        } else {
            response.render('client/edit.html', {
                controller: 'client',
                client: client,
                cameras: cameras,
                settops: result.rows
            });
        }
    });
});

router.post('/client/save', function(request, response, next) {
    var connection = null;
    var done = null;
    var client = null;

    var id = request.body.id;

    var name = request.body.name.trim();
    var password = request.body.password.trim();
    var email = request.body.email.trim();
    var representative = request.body.representative.trim();
    var telephone = request.body.telephone.trim();
    var cellphone = request.body.cellphone.trim();
    var note = request.body.note.trim();

    var domainName = request.body.domain_name.trim();

    var column = parseInt(request.body.column.trim());
    var row = parseInt(request.body.row.trim());
    var network = !!request.body.network.trim();

    var monitor = !!request.body.monitor.trim();
    var place = request.body.place.trim();

    var lock = !!request.body.lock;

    var settopId = (request.body.settop_id || '').trim();
    if (!id) {
        var device1 = (request.body.device1 || '').trim();
        var device2 = (request.body.device2 || '').trim();
        var recorderIp = (request.body.recorder_ip || '').trim();
        var cameraCount = parseInt((request.body.camera_count || '').trim());
    }

    var option = {live: {}};
    if (column) {
        option.live.column = column;
    }
    if (row) {
        option.live.row = row;
    }
    if (network) {
        option.network = network;
    }

    async.waterfall([
        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

            connection.query('BEGIN');
            if (id) {
                connection.query('SELECT * FROM settop WHERE client_id = $1 LIMIT 1', [id], function(error, result) {
                    var settop = result.rows[0];

                    connection.query('SELECT camera.id, stream.id AS stream_id FROM camera LEFT JOIN stream ON stream.type = \'main\' AND stream.camera_id = camera.id WHERE camera.recorder_id IN (SELECT id FROM recorder WHERE settop_id IN (SELECT id FROM settop WHERE client_id = $1)) ORDER BY channel', [id], function(error, result) {
                        result.rows.forEach(function(row, index) {
                            channel = (index + 1).toString();

                            var name = request.body['name' + channel].trim();
                            var frame = request.body['frame' + channel].trim() || 0;
                            var bitrate = request.body['bitrate' + channel].trim() || 0;
                            var place = request.body['place' + channel].trim() || '';
                            var cameraResolution = request.body['camera_resolution' + channel].trim() || '0x0';
                            var streamResolution = request.body['stream_resolution' + channel].trim() || '0x0';

                            connection.query('UPDATE camera SET name = $2, place = $3, resolution = $4 WHERE id = $1', [row.id, name, place, cameraResolution]);

                            if (!row.stream_id) {
                                connection.query('INSERT INTO stream (camera_id, frame, resolution, bitrate) VALUES ($1, $2, $3, $4)', [row.id, frame, streamResolution, bitrate]);
                            } else {
                                connection.query('UPDATE stream SET frame = $2, resolution = $3, bitrate = $4 WHERE id = $1', [row.stream_id, frame, streamResolution, bitrate]);
                            }
                        });

                        if (settopId && settop.id != settopId) {
                            connection.query('UPDATE settop new SET lock = true, key = old.key, device1 = old.device1, device2 = old.device2, client_id = old.client_id, status = \'done\' FROM settop old WHERE new.id = $1 AND old.id = $2', [settopId, settop.id]);
                            connection.query('UPDATE recorder SET settop_id = $1 WHERE settop_id = $2', [settopId, settop.id]);
                            connection.query('UPDATE log SET source_id = $1 WHERE source_id = $2', [settopId, settop.id]);
                            connection.query('UPDATE settop SET status = \'closed\', client_id = null WHERE id = $1', [settop.id]);
                        }

                        if (password) {
                            connection.query('UPDATE "user" SET email = $1, password = md5($2) WHERE id = (SELECT user_id FROM client_user WHERE client_id = $3)', [email, password, id]);
                        } else {
                            connection.query('UPDATE "user" SET email = $1 WHERE id = (SELECT user_id FROM client_user WHERE client_id = $2)', [email, id]);
                        }

                        connection.query('UPDATE client SET name = $1, option = $2, representative = $4, telephone = $5, cellphone = $6, note = $7, monitor = $8, place = $9 WHERE id = $3', [name, option, id, representative, telephone, cellphone, note, monitor, place]);
                        connection.query('UPDATE domain SET name = $1 WHERE client_id = $2', [domainName, id]);
                        connection.query('UPDATE settop SET lock = $1 WHERE client_id = $2', [lock, id]);
                        connection.query('COMMIT', next);
                    });
                });
            } else {
                id = uuid.v4();
                var userId = uuid.v4();
                var recorderId = uuid.v4();
                connection.query('SELECT id FROM "user" WHERE email = $1', [email], function(error, result) {
                    if (error) {
                        return next(error);
                    }

                    if (!password) {
                        password = email;
                    }

                    if (result.rows.length) {
                        userId = result.rows[0].id;
                        connection.query('UPDATE "user" SET password = md5($1) WHERE id = $2', [password, userId]);
                    } else {
                        connection.query('INSERT INTO "user"(id, type, status, email, password) VALUES($1, $2, $3, $4 , md5($5))', [userId, 'owner', 'success', email, password]);
                    }

                    connection.query('INSERT INTO client(id, name, representative, telephone, cellphone, note, option, monitor, place) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9)', [id, name, representative, telephone, cellphone, note, option, monitor, place]);
                    connection.query('INSERT INTO client_user(client_id, user_id, type, create_date, update_date) VALUES($1, $2, \'owner\', now(), now())', [id, userId]);
                    connection.query('INSERT INTO domain(client_id, name) VALUES($1, $2)', [id, domainName]);
                    connection.query('UPDATE settop SET client_id = $1, status = \'done\', device1 = $3, device2 = $4, lock = $5 WHERE id = $2', [id, settopId, device1, device2, lock]);
                    connection.query('INSERT INTO recorder(id, settop_id, ip, "user", password) VALUES($1, $2, $3, $4, $5)', [recorderId, settopId, recorderIp, 'admin', 'iphost2013']);
                    for (var i = 0; i < cameraCount; i++) {
                        connection.query('INSERT INTO camera(recorder_id, channel) VALUES($1, $2)', [recorderId, i + 1]);
                    }
                    connection.query('COMMIT', next);
                });
            }
        },

        function(result, next) {
            next();
        }
    ], function(error) {
        if (done) {
            done();
        }
        if (error) {
            logger.error(error);
            response.redirect('/');
        } else {
            response.redirect('/client/edit?id=' + id);
        }
    });
});

router.get('/client/delete', function(request, response, next) {
    var connection = null;
    var done = null;
    var client = null;

    var id = request.query.id;

    async.waterfall([
        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

            connection.query('BEGIN');
            connection.query('DELETE FROM "user" WHERE id = (SELECT user_id FROM client_user WHERE client_id = $1)', [id]);
            connection.query('DELETE FROM client WHERE id = $1', [id]);
            connection.query('COMMIT', next);
        }
    ], function(error, result) {
        if (done) {
            done();
        }
        if (error) {
            logger.error(error);
            response.redirect('/');
        } else {
            response.redirect('/client/list');
        }
    });
});
