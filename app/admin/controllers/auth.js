var express = require('express');
var router = module.exports = express.Router();

router.get('/auth/login', function(request, response, next) {
    response.render('auth/login.html');
});

router.post('/auth/login', function(request, response, next) {
    if (request.body.email == 'admin@cctm.kr' && request.body.password == 'cctm!@34') {
        request.session.logged = true;
        response.redirect('/');
    } else if (request.body.email == 'root@cctm.kr' && request.body.password == 'iphost!@34') {
        request.session.logged = true;
        request.session.userId = 'root@cctm.kr';
        response.redirect('/');
    } else {
        response.render('auth/login.html');
    }
});

router.get('/auth/logout', function(request, response, next) {
    delete request.session.logged;
    delete request.session.userId;
    response.redirect('/');
});
