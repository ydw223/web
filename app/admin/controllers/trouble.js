var pg = require('pg');
var uuid = require('uuid');
var async = require('async');
var config = require('config');
var logger = require('logger');
var heredoc = require('heredoc');
var express = require('express');
var router = module.exports = express.Router();

router.get('/trouble/list', function(request, response, next) {
    var connection = null;
    var done = null;
    var clients = null;

    async.waterfall([
        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

            connection.query(heredoc(function() {/*
                SELECT
                    c.id,
                    c.name,
                    u.email,
                    s.id AS settop_id,
                    s.ip,
                    s.public_ip,
                    s.private_ip,
                    s.online,
                    s.lock,
                    s.pending,
                    s.online_date,
                    s.offline_date,
                    s.version AS settop_version,
                    r.id AS recorder_id,
                    r.ip AS recorder_ip,
                    r.version AS recorder_version,
                    r.firmware AS firmware_version,
                    d.name AS domain_name
                FROM client c, client_user cu, "user" u, settop s, recorder r, domain d
                WHERE
                    c.id = cu.client_id AND u.id = cu.user_id ANd c.id = s.client_id AND s.id = r.settop_id and d.client_id = c.id
                    AND
                    cu.type = 'owner'
                    AND
                    s.public_ip != '1.233.82.73' 
                    AND
                    c.name NOT LIKE '%-회수'
                    AND
                    c.name NOT LIKE '%-폐원'
                    AND
                    c.name NOT LIKE '%-폐원철거'
                    AND
                    s.id IN (
                        SELECT settop_id
                        FROM recorder
                        WHERE id IN (
                            SELECT distinct(recorder_id) FROM camera WHERE id NOT IN (
                                SELECT distinct(camera_id) FROM file WHERE end_date > now() - interval '9 hours'
                            )
                        )
                    )
                ORDER BY s.create_date DESC
            */}), next);
        },

        function(result, next) {
            clients = result.rows;
            next();
        }
    ], function(error) {
        if (done) {
            done();
        }
        if (error) {
            logger.error(error);
            response.redirect('/');
        } else {
            response.render('trouble/list.html', {
                controller: 'trouble',
                clients: clients
            });
        }
    });
});
