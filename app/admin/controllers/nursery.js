var pg = require('pg');
var xlsx = require('xlsx');
var util = require('util');
var async = require('async');
var config = require('config');
var logger = require('logger');
var heredoc = require('heredoc');
var express = require('express');
var router = module.exports = express.Router();

router.all('/nursery/list', function(request, response, next) {
    response.render('nursery/list.html', {
        controller: 'nursery'
    });
});
