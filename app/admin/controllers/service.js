var pg = require('pg');
var fs = require('fs');
var xlsx = require('xlsx');
var util = require('util');
var async = require('async');
var moment = require('moment');
var config = require('config');
var logger = require('logger');
var heredoc = require('heredoc');
var express = require('express');
var router = module.exports = express.Router();

var size = 5;
var limit = 30;

var getQuery = function(params, sorting, paging) {
    var status = params.status;
    var manager = params.manager;
    var install = params.install;
    var cameraCount = params.cameraCount;
    var deposit = params.deposit;
    var process = params.process;
    var search = params.search;
    var page = params.page;
    var sort = params.sort || '';

    var index = 0;
    var fields = ["service_application.status != 'delete'"];
    var values = [];

    if (status) {
        index++;
        fields.push('service_application.status = $' + index);
        values.push(status);
    }

    if (manager !== undefined && manager != '지사전체') {
        index++;
        if (manager) {
            fields.push('manager = $' + index);
        } else {
            fields.push('(manager = $' + index + ' OR manager IS NULL)');
        }
        values.push(manager);
    } else {
        if (params.manager === undefined) {
            params.manager = '지사전체';
        }
    }

    if (install !== undefined && install != '설치여부전체') {
        index++;
        if (install) {
            fields.push('install = $' + index);
        } else {
            fields.push('(install = $' + index + ' OR install IS NULL)');
        }
        values.push(install);
    } else {
        if (params.install === undefined) {
            params.install = '설치여부전체';
        }
    }

    if (cameraCount !== undefined && cameraCount != '카메라대수전체') {
        index++;
        fields.push('service_application.camera_count = $' + index);
        values.push(cameraCount);
    } else {
        if (params.cameraCount === undefined) {
            params.cameraCount = '카메라대수전체';
        }
    }

    if (deposit !== undefined && deposit != '입금여부전체') {
        index++;
        if (deposit) {
            fields.push('deposit = $' + index);
        } else {
            fields.push('(deposit = $' + index + ' OR deposit IS NULL)');
        }
        values.push(deposit);
    } else {
        if (params.deposit === undefined) {
            params.deposit = '입금여부전체';
        }
    }

    if (process !== undefined && process != '서류처리전체') {
        index++;
        if (process) {
            fields.push('process = $' + index);
        } else {
            fields.push('(process = $' + index + ' OR process IS NULL)');
        }
        values.push(process);
    } else {
        if (params.process === undefined) {
            params.process = '서류처리전체';
        }
    }

    if (search) {
        index++;
        search = search.replace(/-/g, '').trim();

        var conditions = [
            'number::text',
            'nursery_name',
            'nursery_address',
            'applicant_name',
            'applicant_email',
            'ejboo_id',
            "replace(applicant_phone, '-', '')",
            'note'
        ];
        var sql = " ILIKE '%' || $" + index + " || '%'";

        fields.push('(' + conditions.join(sql + ' OR ') + sql + ')');
        values.push(search);
    }

    if (sorting) {
        sort = 'ORDER BY ' + sort;
    } else {
        sort = '';
    }

    if (paging) {
        page = 'OFFSET ' + ((page - 1) * limit) + ' LIMIT ' + limit;
    } else {
        page = '';
    }

    return {
        text: util.format(heredoc(function() {/*
            FROM service_application LEFT JOIN nursery ON service_application.nursery_id = nursery.id
            WHERE
            %s
            %s
        */}), fields.join(' AND '), sort, page || ''),

        values: values
    };
};

var getListQuery = function(params, paging) {
    var query = getQuery(params, true, paging);
    query.text = 'SELECT service_application.*, nursery.address, nursery.name, nursery.phone, nursery.fax ' + query.text;
    return query;
};

var getCountQuery = function(params) {
    var query = getQuery(params);
    query.text = 'SELECT count(*) AS count ' + query.text;
    return query;
};

router.all(/^\/service\/(list|download).*/, function(request, response, next) {
    if (request.query.init) {
        request.cookies.service = {};
        response.cookie('service', {});
    } else {
        var service = request.cookies.service || {};

        ['search', 'status', 'manager', 'install', 'cameraCount', 'deposit', 'process', 'page'].forEach(function(name) {
            if (request.query[name] !== undefined) {
                service[name] = request.query[name];
            }
            if (request.body[name] !== undefined) {
                service[name] = request.body[name];
            }
        });

        request.cookies.service = service;
        response.cookie('service', service);
    }

    next();
});

router.all('/service/list', function(request, response, next) {
    var params = request.cookies.service;
    var connection = null;
    var done = null;

    var estimates = null;
    var calculates = null;

    if (!params.page) {
        params.page = 1;
    }
    if (params.page <= 0) {
        params.page = 1;
    }
    if (!params.sort) {
        params.sort = 'create_date desc';
    }

    async.waterfall([
        function(next) {
            async.parallel({
                estimates: function(done) {
                    fs.readdir(__dirname + '/../excels/견적서', done);
                },

                calculates: function(done) {
                    fs.readdir(__dirname + '/../excels/정산서', done);
                }
            }, next);
        },

        function(results, next) {
            estimates = results.estimates;
            calculates = results.calculates;
            next();
        },

        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

            var query = getCountQuery(params);
            connection.query(query.text, query.values, next);
        },

        function(result, next2) {
            var count = parseInt(result.rows[0].count);
            var last = Math.floor((count + (limit - 1)) / limit);
            if (last <= 0) {
                last = 1;
            }

            var page = parseInt(params.page || 1);
            if (page > last) {
                page = last;
            }
            if (page <= 0) {
                page = 1;
            }

            var start = (((page - 1) / size) * size) + 1;
            var start = page - Math.floor(size / 2);
            if (start <= 0) {
                start = 1;
            }

            if (end - start > size) {
                start = end - size;
                if (start <= 0) {
                    start = 1;
                }
            }

            var end = start + size - 1;
            if (end > last) {
                end = last;
                start = end - size + 1;
                if (start <= 0) {
                    start = 1;
                }
            }
            if (end <= 0) {
                end = 1;
            }

            var prev = page - 1;
            if (prev <= 0) {
                prev = 1;
            }

            var next = page + 1;
            if (next > last) {
                next = last;
            }
            if (next <= 0) {
                next = 1;
            }

            params.count = count;
            params.page = page;
            params.start = start;
            params.end = end;
            params.first = 1;
            params.last = last;
            params.prev = prev;
            params.next = next;

            next2();
        },

        function(next) {
            var query = getListQuery(params, true);
            connection.query(query.text, query.values, next);
        }
    ], function(error, result) {
        if (done) {
            done();
        }
        if (error) {
            logger.error(error);
            response.redirect('/');
        } else {
            params.controller = 'service';
            params.serviceApplications = result.rows;
            params.estimates = estimates;
            params.calculates = calculates;
            response.render('service/list.html', params);
        }
    });
});

router.all('/service/download', function(request, response, next) {
    var params = request.cookies.service;
    var connection = null;
    var done = null;

    if (!params.sort) {
        params.sort = 'create_date desc';
    }

    async.waterfall([
        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

            if (!params.sort) {
                params.sort = 'create_date desc';
            }

            var query = getListQuery(params, false);
            connection.query(query.text, query.values, next);
        },

        function(result, next) {
            var index = 2;
            var cells = {'A': {}, 'B': {}, 'C': {}, 'D': {}, 'E': {}, 'F': {}, 'G': {}, 'H': {}, 'I': {}, 'J': {}, 'K': {}, 'L': {}, 'M': {}, 'N': {}, 'O': {}, 'P': {}, 'Q': {}, 'R': {}};

            async.eachSeries(result.rows, function(row, next) {
                if (!row.manager) {
                    row.manager = '없음';
                }
                if (!row.install) {
                    row.install = '모름';
                }
                if (row.camera_count === null) {
                    row.camera_count = '모름';
                }

                var number = row.number + '';
                var status = '';

                if (row.status == 'submit') {
                    status = '신청';
                } else if (row.status == 'accept') {
                    status = '접수';
                } else if (row.status == 'reserve') {
                    status = '실사예약';
                } else if (row.status == 'inspect') {
                    status = '실사완료';
                } else if (row.status == 'contract') {
                    status = '설치예약';
                } else if (row.status == 'install') {
                    status = '개통완료';
                } else if (row.status == 'close') {
                    status = '파기';
                }

                cells.A[index] = {value: number.substr(0, 4) + '-' + number.substr(4, 4)};
                cells.B[index] = {value: status};
                cells.C[index] = {value: row.nursery_name};
                cells.D[index] = {value: row.nursery_address};
                cells.E[index] = {value: row.applicant_name};
                cells.F[index] = {value: row.applicant_email};
                cells.G[index] = {value: row.applicant_phone};
                cells.H[index] = {value: moment(row.create_date).format('YYYY-MM-DD HH:mm:ss')};
                cells.I[index] = {value: row.as_period};
                cells.J[index] = {value: row.ejboo_id};
                cells.K[index] = {value: row.manager};
                cells.L[index] = {value: row.install};
                cells.M[index] = {value: row.camera_count};
                cells.N[index] = {value: row.camera_pixel};
                cells.O[index] = {value: row.travel_expense};
                cells.P[index] = {value: (row.note || '').trim()};
                var zipcode = '';
                if (row.address) {
                    var match = row.address.match(/^\((\d){5,6}\)/, '');
                    if (!match || !match.length) {
                        match = row.address.match(/^\(\d{3}-\d{3}\)/, '')
                    }
                    if (match && match.length) {
                        zipcode = match[0].replace('(', '').replace(')', '');
                    }
                }
                cells.Q[index] = {value: zipcode};
                cells.R[index] = {value: (row.deposit || '').trim()};

                index++;
                setImmediate(next);
            }, function() {
                var workbook = {
                    template: __dirname + '/../excels/service.xls',
                    format: 'excel2007',
                    sheets: [
                        {cells: cells}
                    ]
                };
                next(null, workbook);
            });
        }
    ], function(error, workbook) {
        if (done) {
            done();
        }
        if (error) {
            logger.error(error);
            response.redirect('/');
        } else {
            var fileName = encodeURIComponent('서비스신청목록_' + moment(new Date()).format('YYYYMMDDHHmmss') + '.xlsx');
            response.setHeader('Cache-Control', 'max-age=0');
            response.setHeader('Content-Type', 'application/vnd.ms-excel; charset=utf-8');
            response.setHeader('Content-Disposition', 'attachment; filename="' + fileName + '"');

            var stream = xlsx.createReadStream(workbook);
            stream.pipe(response);
            stream.on('error', function(error) {
                logger.error(error);
                response.status(500).end();
            });
        }
    });
});

router.get('/service/estimate', function(request, response, next) {
    var id = request.query.id;
    var estimate = request.query.estimate;

    var connection = null;
    var done = null;

    async.waterfall([
        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

            connection.query(heredoc(function() {/*
                UPDATE service_application
                SET estimate = $1
                WHERE id = $2
            */}), [estimate, id], next);
        },

        function(result, next) {
            connection.query(heredoc(function() {/*
                SELECT
                    service_application.nursery_name,
                    service_application.nursery_address,
                    service_application.camera_count,
                    service_application.camera_pixel,
                    service_application.open_date,
                    service_application.as_period,
                    service_application.applicant_name,
                    service_application.applicant_phone,
                    service_application.applicant_email,
                    service_application.install,
                    nursery.type,
                    nursery.representative,
                    nursery.phone,
                    nursery.address,
                    nursery.child_count,
                    nursery.limit_count,
                    nursery.classroom_count
                FROM service_application
                LEFT JOIN nursery ON service_application.nursery_id = nursery.id
                WHERE service_application.id = $1
                LIMIT 1
            */}), [id], next);
        }
    ], function(error, result) {
        if (done) {
            done();
        }
        if (error) {
            logger.error(error);
            response.redirect('/');
        } else {
            var row = result.rows[0];

            var cells1 = {'A': {}, 'G': {}};
            var cells2 = {'B': {}};

            cells1.A[4] = {value: row.nursery_name};
            cells1.G[12] = {value: row.camera_count};

            cells2.B[1] = {value: row.type};
            cells2.B[2] = {value: row.representative};
            cells2.B[3] = {value: row.phone};
            cells2.B[4] = {value: (row.address || row.nursery_address || '').replace(/^\((\d){5,6}\)\s+/, '').replace(/^\(\d{3}-\d{3}\)\s+/, '')};
            cells2.B[5] = {value: row.limit_count};
            cells2.B[6] = {value: row.child_count};
            if (row.open_date) {
                cells2.B[7] = {value: moment(row.open_date).format('YYYY-MM-DD')};
            }
            cells2.B[8] = {value: row.classroom_count};
            cells2.B[9] = {value: row.as_period};
            cells2.B[10] = {value: row.applicant_phone};
            cells2.B[11] = {value: row.applicant_email};
            cells2.B[12] = {value: row.applicant_name};
            cells2.B[13] = {value: row.camera_pixel};

            var install = '미설치';
            if (row.install == '기설치') {
                install = '기설치';
            }

            var workbook = {
                template: __dirname + '/../excels/견적서/' + estimate + '/' + estimate + '_CCTV_견적서_' + install + '.xlsx',
                format: 'excel2007',
                sheets: [
                    {cells: cells1},
                    {cells: cells2}
                ]
            };

            var fileName = encodeURIComponent(row.nursery_name + '_견적서.xlsx');
            response.setHeader('Cache-Control', 'max-age=0');
            response.setHeader('Content-Type', 'application/vnd.ms-excel; charset=utf-8');
            response.setHeader('Content-Disposition', 'attachment; filename="' + fileName + '"');

            var stream = xlsx.createReadStream(workbook);
            stream.pipe(response);
            stream.on('error', function(error) {
                logger.error(error);
                response.status(500).end();
            });
        }
    });
});

router.get('/service/calculate', function(request, response, next) {
    var id = request.query.id;
    var calculate = request.query.calculate;

    var connection = null;
    var done = null;

    async.waterfall([
        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

            connection.query(heredoc(function() {/*
                UPDATE service_application
                SET calculate = $1
                WHERE id = $2
            */}), [calculate, id], next);
        },

        function(result, next) {
            connection.query(heredoc(function() {/*
                SELECT
                    service_application.nursery_name,
                    service_application.nursery_address,
                    service_application.camera_count,
                    service_application.camera_pixel,
                    service_application.open_date,
                    service_application.as_period,
                    service_application.applicant_name,
                    service_application.applicant_phone,
                    service_application.applicant_email,
                    service_application.install,
                    nursery.type,
                    nursery.representative,
                    nursery.phone,
                    nursery.address,
                    nursery.child_count,
                    nursery.limit_count,
                    nursery.classroom_count
                FROM service_application
                LEFT JOIN nursery ON service_application.nursery_id = nursery.id
                WHERE service_application.id = $1
                LIMIT 1
            */}), [id], next);
        }
    ], function(error, result) {
        if (done) {
            done();
        }
        if (error) {
            logger.error(error);
            response.redirect('/');
        } else {
            var row = result.rows[0];

            var cells1 = {};
            var cells2 = {'B': {}};

            cells2.B[1] = {value: row.type};
            cells2.B[2] = {value: row.representative};
            cells2.B[3] = {value: row.phone};
            cells2.B[4] = {value: (row.address || row.nursery_address || '').replace(/^\((\d){5,6}\)\s+/, '').replace(/^\(\d{3}-\d{3}\)\s+/, '')};
            cells2.B[5] = {value: row.limit_count};
            cells2.B[6] = {value: row.child_count};
            if (row.open_date) {
                cells2.B[7] = {value: moment(row.open_date).format('YYYY-MM-DD')};
            }
            cells2.B[8] = {value: row.classroom_count};
            cells2.B[9] = {value: row.as_period};
            cells2.B[10] = {value: row.applicant_phone};
            cells2.B[11] = {value: row.applicant_email};
            cells2.B[12] = {value: row.applicant_name};
            cells2.B[13] = {value: row.camera_pixel};

            var install = '미설치';
            if (row.install == '기설치') {
                install = '기설치';
            }

            var workbook = {
                template: __dirname + '/../excels/정산서/' + calculate + '/' + calculate + '_CCTV_정산서.xls',
                format: 'excel5',
                sheets: [
                    {cells: cells1},
                    {cells: cells2}
                ]
            };

            var fileName = encodeURIComponent(row.nursery_name + '_정산서.xls');
            response.setHeader('Cache-Control', 'max-age=0');
            response.setHeader('Content-Type', 'application/vnd.ms-excel; charset=utf-8');
            response.setHeader('Content-Disposition', 'attachment; filename="' + fileName + '"');

            var stream = xlsx.createReadStream(workbook);
            stream.pipe(response);
            stream.on('error', function(error) {
                logger.error(error);
                response.status(500).end();
            });
        }
    });
});

router.get('/service/edit', function(request, response, next) {
    var id = request.query.id;
    var connection = null;
    var done = null;

    var estimates = null;
    var calculates = null;
    var application = null;

    async.waterfall([
        function(next) {
            async.parallel({
                estimates: function(done) {
                    fs.readdir(__dirname + '/../excels/견적서', done);
                },

                calculates: function(done) {
                    fs.readdir(__dirname + '/../excels/정산서', done);
                }
            }, next);
        },

        function(results, next) {
            estimates = results.estimates;
            calculates = results.calculates;
            next();
        },

        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

            connection.query(heredoc(function() {/*
                SELECT *
                FROM service_application
                WHERE id = $1
                LIMIT 1
            */}), [id], next);
        },

        function(result, next) {
            application = result.rows[0];
            if (!application) {
                return next(null, []);
            }

            var name = application.nursery_name.replace(/ /g, '');
            var tokens = application.nursery_address.split(' ');
            var address = [tokens[0], tokens[1]].join(' ');
            address = address.replace(/\d*$/g, '');

            connection.query(heredoc(function() {/*
                SELECT *
                FROM nursery
                WHERE replace(name, ' ', '') LIKE $1 || '%' AND regexp_replace(address, '^\([0-9-]*\) ', '') LIKE $2 || '%'
            */}), [name, address], next);
        },

        function(result, next) {
            if (result.rows.length) {
                return next(null, result);
            }

            var name = application.nursery_name.replace(/ /g, '');
            var address1 = application.nursery_address.trim().substr(0, 4);
            if (address1.indexOf(' ') > 0) {
                address1 = address1.substr(0, 2);
            }

            var address2 = address1;
            if (address1 == '충남') {
                address2 = '충청남도';
            } else if (address1 == '충북') {
                address2 = '충청북도';
            } else if (address1 == '전남') {
                address2 = '전라남도';
            } else if (address1 == '전북') {
                address2 = '전라북도';
            } else if (address1 == '경남') {
                address2 = '경상남도';
            } else if (address1 == '경북') {
                address2 = '경상북도';
            }

            connection.query(heredoc(function() {/*
                SELECT *
                FROM nursery
                WHERE replace(name, ' ', '') LIKE $1 || '%' AND (regexp_replace(address, '^\([0-9-]*\) ', '') LIKE $2 || '%' OR regexp_replace(address, '^\([0-9-]*\) ', '') LIKE $3 || '%')
                ORDER by address
            */}), [name, address1, address2], next);
        }
    ], function(error, result) {
        if (done) {
            done();
        }
        if (error) {
            logger.error(error);
            response.redirect('/');
        } else {
            response.render('service/edit.html', {
                controller: 'service',
                estimates: estimates,
                calculates: calculates,
                serviceApplication: application,
                nurseries: result.rows
            });
        }
    });
});

router.get('/service/delete', function(request, response, next) {
    var id = request.query.id;
    var connection = null;
    var done = null;

    async.waterfall([
        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

            connection.query(heredoc(function() {/*
                UPDATE service_application
                SET status = 'delete'
                WHERE id = $1
            */}), [id], next);
        }
    ], function(error, result) {
        if (done) {
            done();
        }
        if (error) {
            logger.error(error);
            response.redirect('/');
        } else {
            response.redirect('/service/list');
        }
    });
});

router.post('/service/save', function(request, response, next) {
    var id = request.body.id;
    var status = request.body.status;
    var nurseryName = request.body.nursery_name;
    var nurseryAddress = request.body.nursery_address;
    var applicantName = request.body.applicant_name;
    var applicantEmail = request.body.applicant_email;
    var applicantPhone = request.body.applicant_phone;
    var ejbooId = request.body.ejboo_id;
    var manager = request.body.manager;
    var install = request.body.install;
    var cameraCount = request.body.camera_count;
    var cameraPixel = request.body.camera_pixel;
    var travelExpense = request.body.travel_expense;
    var asPeriod = request.body.as_period;
    var openDate = request.body.open_date;
    var deposit = request.body.deposit;
    var process = request.body.process;
    var note = request.body.note;
    var nurseryId = request.body.nursery_id;

    var connection = null;
    var done = null;

    async.waterfall([
        function(next) {
            pg.connect(config.db, next);
        },

        function(result1, result2, next) {
            connection = result1;
            done = result2;

            var sql = heredoc(function() {/*
                UPDATE service_application
                SET status = $1, nursery_name = $2, nursery_address = $3, applicant_name = $4, applicant_email = $5, applicant_phone = $6, ejboo_id = $7, note = $8, manager = $9, install = $10, nursery_id = $11, camera_pixel = $12, travel_expense = $13
            */});
            var values = [status, nurseryName, nurseryAddress, applicantName, applicantEmail, applicantPhone, ejbooId, note, manager, install, nurseryId, cameraPixel, travelExpense];
            var index = 14;

            if (openDate === '') {
                sql += ', open_date = null';
            } else {
                sql += ', open_date = $' + index;
                values.push(openDate);
                index++;
            }

            if (asPeriod === '') {
                sql += ', as_period = null';
            } else {
                sql += ', as_period = $' + index;
                values.push(asPeriod);
                index++;
            }

            if (cameraCount === '') {
                sql += ', camera_count = null';
            } else {
                sql += ', camera_count = $' + index;
                values.push(cameraCount);
                index++;
            }

            if (deposit === '') {
                sql += ', deposit = null';
            } else {
                sql += ', deposit = $' + index;
                values.push(deposit);
                index++;
            }

            if (process === '') {
                sql += ', process = null';
            } else {
                sql += ', process = $' + index;
                values.push(process);
                index++;
            }

            sql += ' WHERE id = $' + index;
            values.push(id);

            connection.query(sql, values, next);
        }
    ], function(error, result) {
        if (done) {
            done();
        }
        if (error) {
            logger.error(error);
            response.redirect('/');
        } else {
            response.redirect('/service/edit?id=' + id);
        }
    });
});
