var express = require('express');
var router = module.exports = express.Router();

router.get('/', function(request, response, next) {
    response.redirect('/service/list');
});
