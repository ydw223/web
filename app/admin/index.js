var config = require('config');
var express = require('express');
var session = require('express-session')
var RedisStore = require('connect-redis')(session);
var app = express();

app.use(session({
    store: new RedisStore({ttl: 86400}),
    secret: 'ejboocctm',
    resave: false,
    cookie: {domain: '.' + config.host},
    saveUninitialized: true
}));
app.use(function(request, response, next) {
    var render = response.render;
    response.render = function(view, param) {
        if (!param) {
            param = {};
        }

        param.session = request.session;
        param.env = process.env;

        response.render = render;
        response.render(view, param);
    }
    next();
});
app.use(require('./controllers/index'));
app.use(function(request, response, next) {
    if (request.path.match(/.html$/)) {
        response.redirect('/');
    } else {
        next();
    }
});
app.use('/jwplayer', require('jwplayer'));
app.use(require('inspinia'));
app.use(require('./controllers/auth'));
app.use(function(request, response, next) {
    if (!request.session.logged && request.url != '/auth/login') {
        response.redirect('/auth/login');
    } else {
        next();
    }
});
app.use(require('./controllers/service'));
app.use(require('./controllers/nursery'));
app.use(require('./controllers/client'));
app.use(require('./controllers/trouble'));

module.exports = app;
