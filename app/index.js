var reload = require('reload');

reload.createWorker(function() {
    process.title = '[CCTM/Web]';
}, function() {
    var pg = require('pg');
    var http = require('http');
    var https = require('https');
    var moment = require('moment');
    var config = require('config');
    var logger = require('logger');
    var express = require('express');
    var nunjucks = require('nunjucks');
    var bodyParser = require('body-parser');
    var cookieParser = require('cookie-parser');

    var app = express();
    var modules = {};

    ['admin', 'admin2', 'client', 'landing'].forEach(function(app) {
        modules[app] = require('./' + app);
        var env = nunjucks.configure(__dirname + '/' + app + '/views', {
            autoescape: false,
            express: modules[app]
        });
        env.addFilter('date', function(date, format) {
            if (!format) {
                format = 'YYYY-MM-DD HH:mm:ss';
            }
            if (!date) {
                return '';
            }
            return moment(date).format(format);;
        });
        env.addFilter('nl2br', function(str) {
            return str.replace(/\r|\n|\r\n/g, '<br />')
        })
    });

    app.set('trust proxy', true);
    app.set('x-powered-by', false);
    app.set('subdomain offset', config.host.split('.').length);

    app.use(express.static(__dirname + '/../public'));
    app.use(cookieParser());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true, parameterLimit: 1000}));

    app.use(function(request, response, next) {
        var host = (request.headers.host || '').split(':').shift();
        if (host.indexOf(config.host) < 0) {
            response.redirect('https://www.' + config.host);
        } else {
            var module = request.subdomains[0];
            if (!module) {
                response.redirect('https://www.' + config.host + request.url);
            } else if (module == 'admin') {
                modules.admin(request, response, next);
            } else if (module == 'admin2') {
                modules.admin2(request, response, next);
            } else if (module == 'www') {
                modules.landing(request, response, next);
            } else if (module == '203-170-119-176') {
                pg.connect(config.db, function(error, connection, done) {
                    if (error) {
                        if (done) {
                            done();
                        }
                        logger.error(error);
                        response.redirect('https://www.' + config.host);
                    } else {
                        connection.query('SELECT client.* FROM domain, client WHERE domain.client_id = client.id AND domain.name = $1 LIMIT 1', ['client1.cctm.kr'], function(error, result) {
                            if (error) {
                                logger.error(error);
                            } else {
                                if (result.rows.length) {
                                    request.client = result.rows[0];
                                    modules.client(request, response, next);
                                } else {
                                    response.redirect('https://www.' + config.host);
                                }
                            }
                            done();
                        });
                    }
                });
            } else {
                pg.connect(config.db, function(error, connection, done) {
                    if (error) {
                        if (done) {
                            done();
                        }
                        logger.error(error);
                        response.redirect('https://www.' + config.host);
                    } else {
                        if (config.ENV != 'production') {
                            host = host.replace(/cctm\.co\.kr$/, 'cctm.kr');
                        }
                        connection.query('SELECT client.* FROM domain, client WHERE domain.client_id = client.id AND domain.name = $1 LIMIT 1', [host], function(error, result) {
                            if (error) {
                                logger.error(error);
                            } else {
                                if (result.rows.length) {
                                    request.client = result.rows[0];
                                    modules.client(request, response, next);
                                } else {
                                    response.redirect('https://www.' + config.host);
                                }
                            }
                            done();
                        });
                    }
                });
            }
        }
    });

    https.createServer(config.tls, app).listen(443, '0.0.0.0');

    http.createServer(function(request, response) {
        var host = (request.headers.host || '').split(':').shift();
        response.statusCode = 302;
        response.setHeader('Location', 'https://' + host + request.url);
        response.end();
    }).listen(80, '0.0.0.0');

    process.on('message', function(message) {
        if (message.type == 'shutdown') {
            process.exit(0);
        }
    });

    process.title = '[CCTM/Web/' + process.pid + ']';
}).run();

