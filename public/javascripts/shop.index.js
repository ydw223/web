$(function() {
    var nurseries = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/shop/nursery?search=%QUERY',
            wildcard: '%QUERY'
        }
    });

    $('input[name=nurseryName]').typeahead(null, {
        display: 'name',
        source: nurseries,
        limit: 120,
        templates: {
            suggestion: Handlebars.compile('<div>{{name}}<br/><small>{{address}}</small></div>')
        }
    }).bind('typeahead:select', function(event, suggestion) {
        $('input[name=nurseryAddress]').val(suggestion.address.replace(/^\((\d){5,6}\)\s+/, '').replace(/^\(\d{3}-\d{3}\)\s+/, ''));
    });

    $('input.tt-hint').remove();

    var init = function() {
        $('input').removeClass('state-error');
        $('label.input').removeClass('state-error');
        $('label.checkbox').removeClass('state-error');
        $('div.note-error').addClass('hide');
    };

    var disable = function() {
        $('input').prop('disabled', 'disabled');
        $('input').addClass('state-disabled');
        $('label.input').addClass('state-disabled');
        $('label.checkbox').addClass('state-disabled');
    };

    var enable = function() {
        $('input').prop('disabled', null);
        $('input').removeClass('state-disabled');
        $('label.input').removeClass('state-disabled');
        $('label.checkbox').removeClass('state-disabled');
    }

    $('.submit').click(function() {
        init();

        var hasError = false;

        var nurseryName = $.trim($('input[name=nurseryName]').val());
        if (!nurseryName) {
            $('div.require-nursery-name').removeClass('hide');
            $('input[name=nurseryName]').parents('label.input').addClass('state-error');
            if (!hasError) {
                $('input[name=nurseryName]').focus();
            }
            hasError = true;
        }

        var nurseryAddress = $.trim($('input[name=nurseryAddress]').val());
        if (!nurseryAddress) {
            $('div.require-nursery-address').removeClass('hide');
            $('input[name=nurseryAddress]').parent('label').addClass('state-error');
            if (!hasError) {
                $('input[name=nurseryAddress]').focus();
            }
            hasError = true;
        }

        var applicantName = $.trim($('input[name=applicantName]').val());
        if (!applicantName || !/^[가-힣a-zA-Z0-9\'\"\-\_ ]{2,20}$/.test(applicantName)) {
            $('div.invalid-applicant-name').removeClass('hide');
            $('input[name=applicantName]').parent('label').addClass('state-error');
            if (!hasError) {
                $('input[name=applicantName]').focus();
            }
            hasError = true;
        }

        var applicantEmail = $.trim($('input[name=applicantEmail]').val());
        if (!applicantEmail || !/[a-zA-Z0-9\.\-\_]+@[a-zA-Z0-9\.\-]/.test(applicantEmail)) {
            $('div.invalid-applicant-email').removeClass('hide');
            $('input[name=applicantEmail]').parent('label').addClass('state-error');
            if (!hasError) {
                $('input[name=applicantEmail]').focus();
            }
            hasError = true;
        }

        var applicantPhone = $.trim($('input[name=applicantPhone]').val());
        if (!applicantPhone || !(/^(0(2|3[1-3]|4[1-4]|5[1-5]|6[1-4]))-(\d{3,4})-(\d{4})$/.test(applicantPhone) || /^(?:(010-\d{4})|(01[1|6|7|8|9]-\d{3,4}))-(\d{4})$/.test(applicantPhone))) {
            $('div.invalid-applicant-phone').removeClass('hide');
            $('input[name=applicantPhone]').parent('label').addClass('state-error');
            if (!hasError) {
                $('input[name=applicantPhone]').focus();
            }
            hasError = true;
        }

        var ejbooId = $.trim($('input[name=ejbooId]').val());

        var agree = $('input[name=agree]:checked').val();
        if (!agree) {
            $('div.require-agree').removeClass('hide');
            $('input[name=agree]').parent('label').addClass('state-error');
            if (!hasError) {
                $('input[name=agree]').focus();
            }
            hasError = true;
        }

        if (!hasError) {
            disable();
            $.ajax({
                url:'/shop/submit',
                method: 'post',
                dataType: 'json',
                data: {
                    nurseryName: nurseryName,
                    nurseryAddress: nurseryAddress,
                    applicantName: applicantName,
                    applicantEmail: applicantEmail,
                    applicantPhone: applicantPhone,
                    ejbooId: ejbooId
                }
            }).done(function(serviceApplication) {
                location.href = '/shop/done?id=' + serviceApplication.id;
            }).error(function() {
                enable();
            });
        }

        return false;
    });
});
