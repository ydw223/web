window.callback = function(event) {
    getPlayer(function(player) {
        if (event.name == 'ready') {
        } else if (event.name == 'load') {
        } else if (event.name == 'click') {
            player.setSelected(event.index);

            if (player.isFullSize()) {
                player.setFullSize(false);
            } else {
                player.setFullSize(true);
            }
        } else if (event.name == 'fullScreen') {
        }
    });
};

$(function() {
    normalSize();

    $('.control .play').click(function() {
        play();
    });

    $('.control .stop').click(function() {
        stop();
    });
    
    $('.control .fullsize').click(function() {
        screenMode = 'full';
        fullSize();
    });

    $('.control .normalsize').click(function() {
        screenMode = 'normal';
        normalSize();
    });

    $('.control .fullscreen').click(function() {
        fullScreen();
    });

    $(document).bind('fullscreenchange', function() {
        if (!$(document).fullScreen()) {
            $('.control').show();
            if (screenMode == 'normal') {
                normalSize();
            } else {
                fullReSize();
            }
        }
    });
});

var playing = false;
var screenMode = 'normal';

function getPlayer(callback) {
    var delay = 100;

    var run = function() {
        var player = window.document.player;

        if (!player) {
            if (document.embeds && document.embeds.player) {
                player = document.embeds.player;
            }
        }

        if (!player) {
            player = document.getElementById('player');
        }

        if (!player) {
            setTimeout(run, delay);
        } else {
            callback(player);
        }
    };

    setTimeout(run, delay);
}

function play() {
    if (format == 'm3u8') {
        getPlayer(function(player) {
            playing = true;

            player.load({column: column, row: row, sources: sources});
            player.play();

            $('.control .play').hide();
            $('.control .stop').show();
        });
    }
}

function stop() {
    if (format == 'm3u8') {
        getPlayer(function(player) {
            playing = false;

            player.stop();
            player.load({column: 0, row: 0, sources: []});

            $('.control .play').show();
            $('.control .stop').hide();
        });
    }
}

function normalReSize() {
    var stageWidth = $('.player-placeholder').parents('.container').width();
    var imgWidth = stageWidth / column;
    var imgHeight = imgWidth / 2;

    if (format != 'm3u8') {
        $('img.camera').width(imgWidth);
        $('img.camera').height(imgHeight);
        $('.player .screen').width(stageWidth + 1);
    } else {
        $('.player .screen').width(stageWidth);
    }
    $('.player .screen').height(imgHeight * row);

    var offset = $('.player-placeholder').offset();

    var $player = $('.player');
    $player.css('left', offset.left);
    $player.css('top', offset.top);

    $('.player-placeholder').height($player.height());
}

function fullReSize() {
    var width = $(window).width();
    if ($('.control:visible').length) {
        var height = $(window).height() - $('.control').height() - 8;
    } else {
        var height = $(window).height();
    }

    if (format == 'm3u8') {
        $('.player .screen').width(width);
    } else {
        $('.player .screen').width(width + 1);
    }
    $('.player .screen').height(height);

    if (format != 'm3u8') {
        var imgWidth = width / column;
        var imgHeight = height / row;

        $('img.camera').width(imgWidth);
        $('img.camera').height(imgHeight);
    }
}

function normalSize() {
    $('#wrapper').show();
    $('.player').show();
    $('.control .normalsize').hide();
    $('.control .fullsize').show();

    var $player = $('.player');
    $player.css('background-color', '#fff');

    var $control = $('.control');
    $control.css('margin-top', '');
    $control.css('margin-left', '');

    $(window).off('resize', normalReSize);
    $(window).off('resize', fullReSize);
    $(window).resize(normalReSize);
    normalReSize();
}

function fullSize() {
    $('#wrapper').hide();
    $('.control .normalsize').show();
    $('.control .fullsize').hide();

    var $player = $('.player');
    $player.css('background-color', '#000');
    $player.css('left', '0px');
    $player.css('top', '0px');

    var $control = $('.control');
    $control.css('margin-top', '5px');
    $control.css('margin-left', '3px');

    $(window).off('resize', normalReSize);
    $(window).off('resize', fullReSize);
    $(window).resize(fullReSize);
    fullReSize();
}

function fullScreen() {
    if ($(document).fullScreen() !== null) {
        $('.control').hide();
        fullSize();
    }

    var element = $('.player')[0];

    if (element.requestFullscreen) {
        if (document.fullScreenElement) {
            document.cancelFullScreen();       
        } else {
            element.requestFullscreen();
        }
    } else if (element.msRequestFullscreen) {
        if (document.msFullscreenElement) {
            document.msExitFullscreen();
        } else {
            element.msRequestFullscreen();
        }
    } else if (element.mozRequestFullScreen) {
        if (document.mozFullScreenElement) {
            document.mozCancelFullScreen();
        } else {
            element.mozRequestFullScreen();
        }
    } else if (element.webkitRequestFullscreen) {
        if (document.webkitFullscreenElement) {
            document.webkitCancelFullScreen();
        } else {
            element.webkitRequestFullscreen();
        }
    } else if (window.ActiveXObject) {
        try {
            var wscript = new ActiveXObject('WScript.Shell');
            if (wscript) {
                wscript.SendKeys('{F11}');
            }
        } catch (e) {
        }
    }
}
