var pg = require('pg');
var async = require('async');
var config = require('config');
var moment = require('moment');

var insertQuery = `
    INSERT INTO management_register(type, date, content, purpose, requester, basis, form, schedule, place, manager, note, client_id)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)
`;

pg.connect(config.db, function(error, connection, done) {
    connection.query('SELECT distinct(client_id) AS client_id FROM management_register', function(error, result) {
        async.eachSeries(result.rows, function(row, next) {
            var client = null;
            var visits = [];
            var cameras = [];

            async.waterfall([
                function(next) {
                    connection.query(`
                        SELECT client.id, "user".id AS user_id, client.representative
                        FROM client, client_user, "user"
                        WHERE
                            client.id = client_user.client_id
                            AND
                            "user".id = client_user.user_id
                            AND
                            client.id = $1
                    `, [row.client_id], next);
                },

                function(result, next) {
                    client = result.rows[0];
                    console.log(client);

                    connection.query(`
                        SELECT id, channel, name
                        FROM camera
                        WHERE recorder_id IN (SELECT id FROM recorder WHERE settop_id IN (SELECT id FROM settop WHERE client_id = $1))
                        ORDER BY channel
                    `, [row.client_id], next);
                },

                function(result, next) {
                    var index = 0;
                    var fields = [];
                    var cameraIds = [];

                    result.rows.forEach(function(camera) {
                        index++;
                        fields.push('$' + index);
                        cameraIds.push(camera.id);
                        cameras.push(camera);
                    });

                    connection.query(`
                        SELECT date FROM visit WHERE user_id = $1 GROUP BY date ORDER BY date
                    `, [client.user_id], next);
                },

                function(result, next) {
                    visits = result.rows;

                    var now = new Date();
                    var date = new Date(2015, 11, 24, 9, 0, 0);
                    var base = new Date(2016, 3, 8, 0, 0, 0);

                    date = moment(date).add(4 - moment(date).format('e'), 'days').toDate();

                    connection.query('BEGIN');
                    //connection.query('DELETE FROM management_register WHERE type = $1 AND client_id = $2', ['파기', row.client_id]);
                    connection.query('DELETE FROM management_register WHERE client_id = $1', [row.client_id]);

                    visits.forEach(function(visit) {
                        connection.query(insertQuery, [
                            '이용',
                            visit.date,
                            '실시간 관제',
                            '영유아보호',
                            client.representative,
                            '',
                            '',
                            moment(visit.date).add(10, 'hours').format('YYYY-MM-DD HH:mm:ss'),
                            '원내',
                            client.representative,
                            '',
                            client.id
                        ]);
                    });
                    
                    while(date < now) {
                        var contents = [];
                        var period = 65;
                        if (date < base) {
                            period = 60;
                        }
                        var formattedDate = moment(date).subtract(period, 'days').format('YYYYMMDD');

                        cameras.forEach(function(camera) {
                            if (!camera.name) {
                                camera.name = 'camera' + camera.channel;
                            }
                            contents.push(camera.name + ', ~' + formattedDate + ' 삭제확인');
                        });

                        connection.query(insertQuery, [
                            '파기',
                            date,
                            contents.join('\n'),
                            '',
                            client.representative,
                            '법15조4의제3항\n표준지침 45조',
                            '',
                            period + '일 주기 자동 파기',
                            '원내',
                            client.representative,
                            '매주 목요일 삭제 확인',
                            client.id
                        ]);

                        date = moment(date).add(7, 'days').toDate();
                    }

                    connection.query('COMMIT', next);

                    /*
                    connection.query('SELECT min(date) AS date FROM management_register WHERE type = $1 AND client_id = $2', ['파기', row.client_id], function(error, result) {
                        if (error) {
                            return next(error);
                        }

                        var date = result.rows[0].date;
                        var formattedDate = moment(date).format('YYYY-MM-DD HH:mm:ss');
                        if (formattedDate > '2015-12-24 09:00:00') {
                            //console.log(row.client_id, formattedDate, date);
                        }

                        next();
                    });
                    */
                }
            ], next);
        }, function(error) {
            if (error) {
                console.error(error);
            }

            process.exit();
        });
    });
});
