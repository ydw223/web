var pg = require('pg');
var async = require('async');
var config = require('config');
var moment = require('moment');

pg.connect(config.db, function(error, connection, done) {
    async.waterfall([
        function(next) {
            connection.query(`
                SELECT client_id, to_char(date, 'YYYYMMDD') AS date
                FROM management_register
                WHERE type = '이용' GROUP BY to_char(date, 'YYYYMMDD'), client_id HAVING count(id) > 1
            `, next);
        },

        function(result, next) {
            connection.query('BEGIN');

            result.rows.forEach(function(row) {
                console.log(row.client_id, row.date);

                connection.query(`
                    UPDATE management_register
                    SET
                        date = (
                            SELECT date FROM management_register WHERE client_id = $1 AND type = '이용' AND to_char(date, 'YYYYMMDD') = $2 ORDER BY date DESC LIMIT 1
                        )
                    WHERE
                        client_id = $3
                        AND
                        to_char(date, 'YYYYMMDD') = $4
                `, [row.client_id, row.date, row.client_id, row.date]);

                connection.query(`
                    DELETE FROM management_register
                    WHERE
                        client_id = $3
                        AND
                        to_char(date, 'YYYYMMDD') = $4
                        AND
                        id NOT IN (
                            SELECT id FROM management_register WHERE client_id = $1 AND type = '이용' AND to_char(date, 'YYYYMMDD') = $2 ORDER BY date ASC LIMIT 1
                        )
                `, [row.client_id, row.date, row.client_id, row.date]);
            });

            connection.query('COMMIT', next);
        }
    ], function(error) {
        if (error) {
            console.error(error);
        }

        done();
        process.exit();
    });
});
