var pg = require('pg');
var async = require('async');
var config = require('config');
var moment = require('moment');

var insertQuery = `
    INSERT INTO management_register(type, date, content, purpose, requester, basis, form, schedule, place, manager, note, client_id)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)
`;

pg.connect(config.db, function(error, connection, done) {
    async.waterfall([
        function(next) {
            connection.query('DELETE FROM management_register', next);
        },

        function(result, next) {
            connection.query(`
                SELECT client.id, "user".id AS user_id, client.representative
                FROM client, client_user, "user"
                WHERE
                    client.id = client_user.client_id
                    AND
                    "user".id = client_user.user_id
                    --AND
                    --client.id = 'e00c392a-e53f-4c9d-8380-7d4f39819e3d'
            `, next);
        },

        function(result, next) {
            async.eachSeries(result.rows, function(row, next) {
                var client = row;
                var cameras = [];

                async.waterfall([
                    function(next) {
                        connection.query(`
                            SELECT date FROM login WHERE user_id = $1 ORDER BY date
                        `, [row.user_id], next);
                    },

                    function(result, next) {
                        async.eachSeries(result.rows, function(login, next) {
                            connection.query(insertQuery, [
                                '이용',
                                login.date,
                                '실시간 관제',
                                '영유아보호',
                                client.representative,
                                '',
                                '',
                                moment(login.date).add(10, 'hours').format('YYYY-MM-DD HH:mm:ss'),
                                '원내',
                                client.representative,
                                '',
                                client.id
                            ], next);
                        }, next);
                    },
                
                    function(next) {
                        connection.query(`
                            SELECT id, channel, name
                            FROM camera
                            WHERE recorder_id IN (SELECT id FROM recorder WHERE settop_id IN (SELECT id FROM settop WHERE client_id = $1))
                            ORDER BY channel
                        `, [client.id], next);
                    },

                    function(result, next) {
                        var index = 0;
                        var fields = [];
                        var cameraIds = [];

                        result.rows.forEach(function(camera) {
                            index++;
                            fields.push('$' + index);
                            cameraIds.push(camera.id);
                            cameras.push(camera);
                        });

                        fields = fields.join(',');

                        connection.query(`
                            SELECT *
                            FROM file
                            WHERE
                                camera_id IN (${fields})
                                AND
                                status = 'deleted'
                            ORDER BY start_date
                            LIMIT 1
                        `, cameraIds, next);
                    },

                    function(result, next) {
                        if (!result.rows.length) {
                            return next();
                        }

                        var startDate = result.rows[0].start_date;
                        startDate = moment(startDate).add(60, 'days').toDate();

                        var now = new Date();
                        var date = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(), 9, 0, 0);
                        date = moment(date).add(4 - moment(date).format('e'), 'days').toDate();

                        connection.query('BEGIN');

                        while(date < now) {
                            var contents = [];
                            var formattedDate = new Date(date);
                            formattedDate = moment(formattedDate).subtract(60, 'days').format('YYYYMMDD');

                            cameras.forEach(function(camera) {
                                if (!camera.name) {
                                    camera.name = 'camera' + camera.channel;
                                }
                                contents.push(camera.name + ', ~' + formattedDate + ' 삭제확인');
                            });

                            connection.query(insertQuery, [
                                '파기',
                                date,
                                contents.join('\n'),
                                '',
                                client.representative,
                                '법15조4의제3항\n표준지침 45조',
                                '',
                                '60일 주기 자동 파기',
                                '원내',
                                client.representative,
                                '매주 목요일 삭제 확인',
                                client.id
                            ]);

                            date = moment(date).add(7, 'days').toDate();
                        }

                        connection.query('COMMIT', next);
                    }
                ], next);
            }, next);
        }
    ], function(error) {
        if (error) {
            console.error(error);
        }

        done();
        process.exit();
    });
});
